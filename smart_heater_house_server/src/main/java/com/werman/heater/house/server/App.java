package com.werman.heater.house.server;

import com.werman.heater.house.server.devices.DevicesManager;
import com.werman.heater.house.server.devices.listeners.HttpDeviceListener;
import com.werman.heater.house.server.devices.senders.HttpDeviceCommandSender;
import com.werman.heater.house.server.remote.PowerOffListener;
import com.werman.heater.house.server.remote.PowerOnListener;
import com.werman.heater.house.server.remote.TargetValueListener;
import com.werman.heater.house.server.sensors.SensorsManager;
import com.werman.heater.house.server.sensors.listeners.HttpSensorListener;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

public class App extends Application {

    public static void main(String[] args) throws Exception {

        Params params = new Params();
        params.init();

        RemoteResources remoteResources = new RemoteResources();
        remoteResources.init(params);

        RemoteAddressManager remoteAddressManager = new RemoteAddressManager(params, remoteResources);
        remoteAddressManager.getExternalIp();

        SecurityManager securityManager = new SecurityManager();
        securityManager.init(params, remoteResources);

        if (!securityManager.isLoggedIn()) {
            securityManager.loginToServer();
        } else {
            securityManager.registerHouse();
        }

        SensorsManager.getInstance().init(remoteResources, securityManager, params);
        DevicesManager.getInstance().init(remoteResources, securityManager, params);
        DevicesManager.getInstance().setCommandSender(new HttpDeviceCommandSender());

        Component component = new Component();
        component.getServers().add(Protocol.HTTP, 8182);

        Application application = new App();

        component.getDefaultHost().attach(application);
        component.start();
    }


    @Override
    public synchronized Restlet createInboundRoot() {
        // Create a router Restlet that routes each call to a new respective instance of resource.
        Router router = new Router(getContext());
        // Defines only three routes
        router.attach("/control/power_on", PowerOnListener.class);
        router.attach("/control/power_off", PowerOffListener.class);
        router.attach("/control/change_target", TargetValueListener.class);
        router.attach("/sensors/data", HttpSensorListener.class);
        router.attach("/devices/data", HttpDeviceListener.class);
        return router;
    }
}
