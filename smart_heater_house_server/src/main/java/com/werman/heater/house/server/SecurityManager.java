package com.werman.heater.house.server;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.werman.heater.house.server.utils.JsonCallback;
import com.werman.heater.house.server.utils.RandomStringGenerator;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SecurityManager {

    private static Logger log = Logger.getLogger(SecurityManager.class.getName());

    private String token;

    private Params params;

    private RemoteResources remoteResources;

    public void init(Params params, RemoteResources remoteResources) {
        log.info("Initializing.");
        this.params = params;
        this.remoteResources = remoteResources;

        if (params.getToken() != null && !params.getToken().isEmpty()) {
            token = params.getToken();
        } else {
            log.info("No token.");
        }
    }

    public void loginToServer() {
        log.info("Login.");

        HttpRequestWithBody r = remoteResources.getObtainTokenResource();
        r.queryString("login", params.getLogin())
                .queryString("password", params.getPassword())
                .asStringAsync(new ObtainTokenResponseCallback());
    }

    protected void registerHouse() {
        log.info("Registering house.");

        String houseId = params.getHouseId();

        if (houseId == null) {
            log.info("No house id - generating.");
            houseId = RandomStringGenerator.generateRandomString(32);
        }

        HttpRequestWithBody r = remoteResources.getRegisterHouseResource();
        r.queryString("houseId", houseId)
                .queryString("url", "http://" + params.getMyIp() + "/")
                .queryString("clientToken", params.getClientToken())
                .asStringAsync(new RegisterHouseResponseCallback(houseId));
    }

    public boolean isLoggedIn() {
        return token != null;
    }

    public String getToken() {
        return token;
    }

    private class ObtainTokenResponseCallback extends JsonCallback<LoginResponse> {

        public ObtainTokenResponseCallback() {
            super(LoginResponse.class);
        }

        @Override
        public void completedJson(JsonResponse<LoginResponse> response) {
            LoginResponse loginResponse = response.getBody();
            token = loginResponse.getToken();
            params.setToken(token);
            params.save();

            log.log(Level.INFO, "Successfully logged in {0}.", token);

            registerHouse();
        }

        @Override
        public void failed(UnirestException e) {
            e.printStackTrace();
            log.log(Level.WARNING, "Login failed");
        }

        @Override
        public void cancelled() {

        }
    }

    private class RegisterHouseResponseCallback extends JsonCallback<LoginResponse> {

        String houseId;

        public RegisterHouseResponseCallback(String houseId) {
            super(LoginResponse.class);
            this.houseId = houseId;
        }

        @Override
        public void completedJson(JsonResponse<LoginResponse> response) {
            params.setHouseId(houseId);
            params.setToken(response.getBody().getToken());
            params.save();
            log.info("House registered successfully.");
        }

        @Override
        public void failed(UnirestException e) {
            log.info("Failed to register house.");
            e.printStackTrace();
        }

        @Override
        public void cancelled() {

        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class LoginResponse {
        @JsonProperty("token")
        private String token;

        @JsonProperty("error")
        private String error;

        public String getToken() {
            return token;
        }

        public String getError() {
            return error;
        }
    }
}
