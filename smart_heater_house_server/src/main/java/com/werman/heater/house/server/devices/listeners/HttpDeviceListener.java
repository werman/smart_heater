package com.werman.heater.house.server.devices.listeners;

import com.werman.heater.house.server.SecuredServerResource;
import com.werman.heater.house.server.devices.DeviceInfo;
import com.werman.heater.house.server.devices.DevicesManager;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;

public class HttpDeviceListener extends SecuredServerResource {

    @Post("application/x-www-form-urlencoded")
    public Representation acceptItem() {
        // Parse the given representation and retrieve data
        Form form = getRequest().getResourceRef().getQueryAsForm();
        String deviceId = form.getFirstValue("id");
        String deviceDesc = form.getFirstValue("device_desc");
        String deviceState = form.getFirstValue("state");
        String devicePort = form.getFirstValue("port");

        String deviceAddress = getRequest().getClientInfo().getAddress() +
                                       ":" + devicePort + "/";

        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setAddress(deviceAddress);
        deviceInfo.setDescription(deviceDesc);

        if (deviceState.equals("on")) {
            deviceInfo.setTurnedOn(true);
        } else {
            deviceInfo.setTurnedOn(false);
        }

        deviceInfo.setId(deviceId);

        DevicesManager.getInstance().setDeviceAlive(deviceInfo);

        return null;
    }
}
