package com.werman.heater.house.server.remote;

import com.werman.heater.house.server.SecuredServerResource;
import com.werman.heater.house.server.devices.DevicesManager;
import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;

public class PowerOffListener extends SecuredServerResource {

    @Post("application/x-www-form-urlencoded")
    public Representation powerOffHeatSource(Representation entity) {
        Form form = getRequest().getResourceRef().getQueryAsForm();
        String deviceId = form.getFirstValue("id");

        DevicesManager.getInstance().sendPowerOff(deviceId);

        getResponse().setStatus(Status.SUCCESS_OK);

        return null;
    }

}
