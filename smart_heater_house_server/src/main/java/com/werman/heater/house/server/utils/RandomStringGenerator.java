package com.werman.heater.house.server.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

public class RandomStringGenerator {

    public static String generateRandomString(int length) {
        SecureRandom random = new SecureRandom();
        return new BigInteger(length * 5, random).toString(length);
    }
}
