package com.werman.heater.house.server.sensors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.werman.heater.house.server.Params;
import com.werman.heater.house.server.RemoteResources;
import com.werman.heater.house.server.SecurityManager;
import com.werman.heater.house.server.utils.JsonCallback;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SensorsManager {

    private static SensorsManager instance = new SensorsManager();

    private static Logger log = Logger.getLogger(SensorsManager.class.getName());

    private RemoteResources remoteResources;

    private SecurityManager securityManager;
    private Params params;

    private Set<String> sensorsInProcess = new HashSet<>();

    private SensorsManager() {}

    public static SensorsManager getInstance() {
        return instance;
    }

    public void init(RemoteResources remoteResources, SecurityManager securityManager, Params params) {
        this.remoteResources = remoteResources;
        this.securityManager = securityManager;
        this.params = params;
    }

    protected void getSensorsFromServer() {

    }

    public void sendSensorDataToServer(String sensorUUID, SensorType sensorType, Float value) {
        if (!securityManager.isLoggedIn()) {
            return;
        }

        if (!checkIfSensorRegistered(sensorUUID, sensorType)) {
            return;
        }

        HttpRequestWithBody resource = null;

        if (sensorType == SensorType.TEMPERATURE)
        {
            resource = remoteResources.getSendSensorDataResource();
        }

        if (resource != null)
        {
            resource.queryString("sensorUUID", params.getSensorRemoteId(sensorUUID).toString())
                    .queryString("houseId", params.getHouseId())
                    .queryString("value", value.toString())
                    .asJsonAsync();
        }
    }

    public boolean checkIfSensorRegistered(String sensorUUID, SensorType sensorType) {
        if (params.getSensorRemoteId(sensorUUID) == null) {
            registerSensorOnServer(sensorUUID, sensorType);
            return false;
        } else {
            return true;
        }
    }

    protected void registerSensorOnServer(String sensorUUID, SensorType sensorType) {

        if (sensorsInProcess.contains(sensorUUID)) {
            log.log(Level.INFO, "Sensor is already in process {0}", sensorUUID);
        }

        log.log(Level.INFO, "Registering sensor on server {0}", sensorUUID);

        if (securityManager.isLoggedIn()) {
            sensorsInProcess.add(sensorUUID);

            HttpRequestWithBody resource = null;

            if (sensorType == SensorType.TEMPERATURE) {
                resource = remoteResources.getRegisterTemperatureSensorResource();
            }

            if (resource != null)
            {
                resource.queryString("sensorUUID", sensorUUID)
                        .queryString("houseId", params.getHouseId())
                        .asStringAsync(new RegisterSensorResponseCallback(sensorUUID));
            }
        }
        else {
            log.log(Level.INFO, "Can't register sensor {0} - not logged in", sensorUUID);
        }
    }

    private class RegisterSensorResponseCallback extends JsonCallback<RegisterSensorResponse> {

        String sensorId;

        public RegisterSensorResponseCallback(String sensorId) {
            super(RegisterSensorResponse.class);
            this.sensorId = sensorId;
        }

        @Override
        public void completedJson(JsonResponse<RegisterSensorResponse> response) {
            RegisterSensorResponse registerSensorResponse = response.getBody();

            if (registerSensorResponse.getSensorId() == -1) {
                log.log(Level.WARNING, "Error while registering sensor {0} Error: {1}",
                               new Object[]{sensorId, registerSensorResponse.getResult()});
            } else {
                params.addRegisteredSensor(sensorId, registerSensorResponse.getSensorId());
                log.log(Level.INFO, "Successfully registered sensor {0}, id - {1}", new Object[]{sensorId, registerSensorResponse.getSensorId()});
            }
            sensorsInProcess.remove(sensorId);
        }

        @Override
        public void failed(UnirestException e) {
            sensorsInProcess.remove(sensorId);
            e.printStackTrace();
        }

        @Override
        public void cancelled() {
            sensorsInProcess.remove(sensorId);
        }
    }

    static class RegisterSensorResponse {

        @JsonProperty("sensor_id")
        private Integer sensorId;
        @JsonProperty("result")
        private String result;

        public Integer getSensorId() {
            return sensorId;
        }

        public String getResult() {
            return result;
        }
    }
}
