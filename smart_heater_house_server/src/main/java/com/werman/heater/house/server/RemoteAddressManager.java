package com.werman.heater.house.server;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;

public class RemoteAddressManager {

    private Params params;
    private RemoteResources remoteResources;

    public RemoteAddressManager(Params params, RemoteResources remoteResources) {
        this.params = params;
        this.remoteResources = remoteResources;
    }

    public void getExternalIp() {
        final GetRequest resource = remoteResources.getObtainRemoteIpResource();

        HttpResponse<String> response = null;
        try {
            response = resource.asString();
            String myIp = response.getBody();
            params.setMyIp(myIp.replace("\n", "") + ":8182");
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
}
