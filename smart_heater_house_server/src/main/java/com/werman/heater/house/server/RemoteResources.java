package com.werman.heater.house.server;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;

public class RemoteResources {

    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

    private Params params;

    public void init(Params params) {
        this.params = params;
    }

    public GetRequest getObtainRemoteIpResource() {
        return Unirest.get("http://checkip.amazonaws.com");
    }

    public HttpRequestWithBody getObtainTokenResource() {
        return Unirest.post(params.getRemoteServerUrl() + "login");
    }

    private HttpRequestWithBody createResource(String path) {
        return Unirest.post(params.getRemoteServerUrl() + path)
                       .header(AUTH_HEADER_NAME, params.getToken());
    }

    public HttpRequestWithBody getRegisterHouseResource() {
        return createResource("house/endpoint");
    }

    public HttpRequestWithBody getRegisterTemperatureSensorResource() {
        return createResource("house/sensors/temperature/");
    }

    public HttpRequestWithBody getSendSensorDataResource() {
        return createResource("house/sensors/temperature/data");
    }

    public HttpRequestWithBody getRegisterDeviceResource() {
        return createResource("house/devices/");
    }

    public HttpRequestWithBody getDeviceStateChangedResource() {
        return createResource("house/devices/state");
    }
}
