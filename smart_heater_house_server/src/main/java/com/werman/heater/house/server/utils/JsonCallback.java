package com.werman.heater.house.server.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Headers;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.async.Callback;

import java.io.IOException;
import java.io.InputStream;

public abstract class JsonCallback<T> implements Callback<String> {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    final Class<T> typeParameterClass;

    public JsonCallback(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }

    @Override
    public void completed(HttpResponse<String> response) {
        try {
            T obj = MAPPER.readValue(response.getBody(), typeParameterClass);
            completedJson(new JsonResponse<T>(response.getStatus(),
                                                     response.getStatusText(),
                                                     response.getHeaders(),
                                                     response.getRawBody(),
                                                     obj));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public abstract void completedJson(JsonResponse<T> response);

    public static class JsonResponse<T> {
        private Integer Status;
        private String statusText;
        private Headers headers = new Headers();
        private InputStream rawBody;
        private T body;

        public JsonResponse(Integer status, String statusText, Headers headers, InputStream rawBody, T body) {
            Status = status;
            this.statusText = statusText;
            this.headers = headers;
            this.rawBody = rawBody;
            this.body = body;
        }

        public Integer getStatus() {
            return Status;
        }

        public String getStatusText() {
            return statusText;
        }

        public Headers getHeaders() {
            return headers;
        }

        public InputStream getRawBody() {
            return rawBody;
        }

        public T getBody() {
            return body;
        }
    }
}
