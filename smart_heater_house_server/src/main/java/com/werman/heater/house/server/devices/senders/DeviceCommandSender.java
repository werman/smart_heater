package com.werman.heater.house.server.devices.senders;

import com.werman.heater.house.server.devices.DeviceInfo;

public interface DeviceCommandSender {

    void sendPowerOn(DeviceInfo deviceInfo);
    void sendPowerOff(DeviceInfo deviceInfo);
    void sendSetTargetValue(DeviceInfo deviceInfo, Float targetValue);
}
