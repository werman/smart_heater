package com.werman.heater.house.server.devices;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.werman.heater.house.server.Params;
import com.werman.heater.house.server.RemoteResources;
import com.werman.heater.house.server.SecurityManager;
import com.werman.heater.house.server.devices.senders.DeviceCommandSender;
import com.werman.heater.house.server.sensors.SensorsManager;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DevicesManager {

    private static DevicesManager instance = new DevicesManager();

    private static Logger log = Logger.getLogger(SensorsManager.class.getName());

    private RemoteResources remoteResources;

    private SecurityManager securityManager;
    private Params params;

    private Set<String> devicesInProcess = new HashSet<>();

    private DeviceCommandSender commandSender;

    private DevicesManager() {
    }

    public static DevicesManager getInstance() {
        return instance;
    }

    public void init(RemoteResources remoteResources, SecurityManager securityManager, Params params) {
        this.remoteResources = remoteResources;
        this.securityManager = securityManager;
        this.params = params;
    }

    public void setCommandSender(DeviceCommandSender deviceCommandSender) {
        commandSender = deviceCommandSender;
    }

    public void setDeviceAlive(DeviceInfo deviceInfo) {
        if (checkIfDeviceRegistered(deviceInfo)) {
            DeviceInfo oldInfo = params.getDeviceInfo(deviceInfo.getId());

            if (deviceInfo.getTurnedOn() != oldInfo.getTurnedOn()) {
                sendDeviceStateChanged(deviceInfo);
            }

            if (!deviceInfo.getAddress().equals(oldInfo.getAddress())) {
                params.addRegisteredDevice(deviceInfo.getId(), deviceInfo);
                params.save();
            }
        }
    }

    public boolean checkIfDeviceRegistered(DeviceInfo deviceInfo) {
        if (params.getDeviceInfo(deviceInfo.getId()) == null) {
            registerDeviceOnServer(deviceInfo);
            return false;
        } else {
            return true;
        }
    }

    protected void registerDeviceOnServer(DeviceInfo deviceInfo) {

        log.log(Level.INFO, "Registering device on server {0}", deviceInfo.getId());

        if (securityManager.isLoggedIn()) {
            deviceInfo.setTurnedOn(false);
            devicesInProcess.add(deviceInfo.getId());

            HttpRequestWithBody resource = remoteResources.getRegisterDeviceResource();

            if (resource != null) {
                resource.queryString("deviceId", deviceInfo.getId())
                        .queryString("houseId", params.getHouseId())
                        .queryString("description", deviceInfo.getDescription())
                        .asStringAsync(new RegisterDeviceResponseCallback(deviceInfo));
            }
        } else {
            log.log(Level.INFO, "Can't register device {0} - not logged in", deviceInfo.getId());
        }
    }

    protected void sendDeviceStateChanged(DeviceInfo deviceInfo) {
        log.log(Level.INFO, "Send state changed {0} state - {1}", new Object[]{deviceInfo.getId(), deviceInfo.getTurnedOn()});

        HttpRequestWithBody resource = remoteResources.getDeviceStateChangedResource();

        String newState = "";

        if (deviceInfo.getTurnedOn()) {
            newState = "on";
        } else {
            newState = "off";
        }

        resource.queryString("deviceId", deviceInfo.getId())
                .queryString("houseId", params.getHouseId())
                .queryString("state", newState)
                .asJsonAsync();

        params.addRegisteredDevice(deviceInfo.getId(), deviceInfo);
        params.save();
    }

    private class RegisterDeviceResponseCallback implements Callback<String> {

        private DeviceInfo deviceInfo;

        public RegisterDeviceResponseCallback(DeviceInfo deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        @Override
        public void completed(HttpResponse<String> response) {
            log.log(Level.INFO, "Successfully registered device {0}", deviceInfo.getId());
            params.addRegisteredDevice(deviceInfo.getId(), deviceInfo);
            params.save();
            devicesInProcess.remove(deviceInfo.getId());
        }

        @Override
        public void failed(UnirestException e) {
            devicesInProcess.remove(deviceInfo.getId());
            e.printStackTrace();
        }

        @Override
        public void cancelled() {
            devicesInProcess.remove(deviceInfo.getId());
        }
    }


    public void sendPowerOn(String deviceId) {
        DeviceInfo deviceInfo = params.getDeviceInfo(deviceId);
        if (deviceInfo != null) {
            commandSender.sendPowerOn(deviceInfo);
        } else {

        }

    }

    public void sendPowerOff(String deviceId) {
        DeviceInfo deviceInfo = params.getDeviceInfo(deviceId);
        if (deviceInfo != null) {
            commandSender.sendPowerOff(deviceInfo);
        } else {

        }
    }

    public void sendSetTargetValue(String deviceId, Float value) {
        DeviceInfo deviceInfo = params.getDeviceInfo(deviceId);
        if (deviceInfo != null) {
            commandSender.sendSetTargetValue(deviceInfo, value);
        } else {

        }
    }
}
