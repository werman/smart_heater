package com.werman.heater.house.server.sensors.listeners;

import com.werman.heater.house.server.SecuredServerResource;
import com.werman.heater.house.server.sensors.SensorType;
import com.werman.heater.house.server.sensors.SensorsManager;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;

public class HttpSensorListener extends SecuredServerResource {

    @Post("application/x-www-form-urlencoded")
    public Representation acceptItem() {
        // Parse the given representation and retrieve data
        Form form = getRequest().getResourceRef().getQueryAsForm();

        String internalId = form.getFirstValue("id");
        String type = form.getFirstValue("type");
        String valueStr = form.getFirstValue("value");

        Float value = Float.parseFloat(valueStr);

        if (value != null) {

            SensorType sensorType = null;

            switch(type) {
                case "temperature":
                    sensorType = SensorType.TEMPERATURE;
                    break;
                case "humidity":
                    sensorType = SensorType.HUMIDITY;
                    break;
            }

            if (sensorType != null) {
                SensorsManager.getInstance().sendSensorDataToServer(internalId, sensorType, value);
            }
        }

        return null;
    }
}
