package com.werman.heater.house.server.sensors;

public enum SensorType {
    TEMPERATURE, HUMIDITY;
}
