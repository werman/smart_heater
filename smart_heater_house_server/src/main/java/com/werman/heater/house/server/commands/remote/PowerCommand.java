package com.werman.heater.house.server.commands.remote;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PowerCommand {

    @JsonProperty("heat_source_id")
    private Integer heatSourceId;

    public Integer getHeatSourceId() {
        return heatSourceId;
    }
}
