package com.werman.heater.house.server;

import org.restlet.Request;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class SecuredServerResource extends ServerResource {

    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

    @Override
    protected Representation doHandle() throws ResourceException {
        Request req = getRequest();
        String token = req.getHeaders().getFirstValue(AUTH_HEADER_NAME);
        if (isTokenValid(token)) {
            return super.doHandle();
        } else {
            doError(Status.CLIENT_ERROR_UNAUTHORIZED);
            return null;
        }
    }

    protected boolean isTokenValid(String token) {
        return true;
    }
}
