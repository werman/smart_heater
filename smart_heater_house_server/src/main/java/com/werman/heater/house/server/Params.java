package com.werman.heater.house.server;

import com.werman.heater.house.server.devices.DeviceInfo;
import com.werman.heater.house.server.utils.RandomStringGenerator;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import java.io.File;

public class Params {

    private static final String SERVER_URL = "remote_url";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String TOKEN = "token";
    private static final String CLIENT_TOKEN = "client_token";
    private static final String HOUSE_ID = "houseId";

    private DB db;

    private HTreeMap<String, String> paramsMap;
    private HTreeMap<String, Integer> sensorsMap;
    private HTreeMap<String, DeviceInfo> devicesMap;

    private String remoteServerUrl;

    private String login;

    private String password;

    private String token;

    private String clientToken;

    private String houseId;

    private String myIp;

    public void init() {
        db = DBMaker.newFileDB(new File("local"))
                        .closeOnJvmShutdown()
                        .make();

        paramsMap = db.getHashMap("params");
        sensorsMap = db.getHashMap("sensors");
        devicesMap = db.getHashMap("devices");

        if(paramsMap.size() == 0) {
            writeDefaults();
        }

        remoteServerUrl = paramsMap.get(SERVER_URL);
        login = paramsMap.get(LOGIN);
        password = paramsMap.get(PASSWORD);
        token = paramsMap.get(TOKEN);
        clientToken = paramsMap.get(CLIENT_TOKEN);
        houseId = paramsMap.get(HOUSE_ID);

        if (clientToken == null) {
            clientToken = RandomStringGenerator.generateRandomString(32);
            paramsMap.put(CLIENT_TOKEN, clientToken);
            save();
        }
    }

    private void writeDefaults() {
        paramsMap.put(SERVER_URL, "http://localhost:8080/api/");
        paramsMap.put(LOGIN, "wermaan@gmail.com");
        paramsMap.put(PASSWORD, "1123581321");
        save();
    }

    public void save() {
        db.commit();
    }

    public String getRemoteServerUrl() {
        return remoteServerUrl;
    }

    public void setRemoteServerUrl(String remoteServerUrl) {
        this.remoteServerUrl = remoteServerUrl;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
        paramsMap.put(LOGIN, login);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        paramsMap.put(PASSWORD, password);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
        paramsMap.put(TOKEN, token);
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
        paramsMap.put(HOUSE_ID, houseId);
    }

    public Integer getSensorRemoteId(String sensorId)
    {
        return sensorsMap.get(sensorId);
    }

    public void addRegisteredSensor(String sensorId, Integer id) {
        sensorsMap.put(sensorId, id);
    }

    public void addRegisteredDevice(String deviceId, DeviceInfo deviceInfo) {
        devicesMap.put(deviceId, deviceInfo);
    }

    public DeviceInfo getDeviceInfo(String deviceId) {
        return devicesMap.get(deviceId);
    }

    public String getMyIp() {
        return myIp;
    }

    public void setMyIp(String myIp) {
        this.myIp = myIp;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
        paramsMap.put(CLIENT_TOKEN, clientToken);
    }
}
