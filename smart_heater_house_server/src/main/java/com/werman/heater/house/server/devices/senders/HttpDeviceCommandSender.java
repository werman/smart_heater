package com.werman.heater.house.server.devices.senders;

import com.mashape.unirest.http.Unirest;
import com.werman.heater.house.server.devices.DeviceInfo;

public class HttpDeviceCommandSender implements DeviceCommandSender {

    @Override
    public void sendPowerOn(DeviceInfo deviceInfo) {
        Unirest.post("http://" + deviceInfo.getAddress() + "control/power")
                .queryString("new_state", "on")
                .asJsonAsync();
    }

    @Override
    public void sendPowerOff(DeviceInfo deviceInfo) {
        Unirest.post("http://" + deviceInfo.getAddress() + "control/power")
                .queryString("new_state", "off")
                .asJsonAsync();
    }

    @Override
    public void sendSetTargetValue(DeviceInfo deviceInfo, Float targetValue) {
        Unirest.post("http://" + deviceInfo.getAddress() + "control/target")
                .queryString("target", targetValue)
                .asJsonAsync();
    }
}
