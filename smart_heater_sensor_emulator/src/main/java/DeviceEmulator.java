import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.restlet.*;
import org.restlet.data.Form;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

public class DeviceEmulator extends Application {

    private String targetValue;
    private String state = "off";
    private Integer port;

    public static void main(String[] args) throws Exception {
        DeviceEmulator application = new DeviceEmulator();

        String deviceId = "";
        application.port = 0;

        if (args.length == 2) {
            deviceId = args[0];
            application.port = Integer.parseInt(args[1]);
        }

        Component component = new Component();
        component.getServers().add(Protocol.HTTP, application.port);

        component.getDefaultHost().attach(application);
        component.start();



        application.startSendingPing(deviceId);
    }

    public void startSendingPing(String deviceId) {
        while (true) {
                System.out.printf("Sending state %s \n", state);
            try {
                Unirest.post("http://localhost:8182/devices/data")
                        .queryString("id", deviceId)
                        .queryString("device_desc", "My-new-device")
                        .queryString("state", state)
                        .queryString("port", port)
                        .asJson();
            } catch (UnirestException e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized Restlet createInboundRoot() {
        // Create a router Restlet that routes each call to a new respective instance of resource.
        Router router = new Router(getContext());

        Restlet changeState = new Restlet(getContext()) {
            @Override
            public void handle(Request request, Response response) {
                Form form = request.getResourceRef().getQueryAsForm();
                String newState = form.getFirstValue("new_state");

                System.out.printf("State change request to %s \n", newState);

                state = newState;
            }
        };

        router.attach("/control/power", changeState);


        Restlet setTarget = new Restlet(getContext()) {
            @Override
            public void handle(Request request, Response response) {
                Form form = request.getResourceRef().getQueryAsForm();
                String newTarget = form.getFirstValue("target");

                //System.out.printf("Change target value to %s", newTarget);

                targetValue = newTarget;
            }
        };

        router.attach("/control/target", setTarget);

        return router;
    }
}