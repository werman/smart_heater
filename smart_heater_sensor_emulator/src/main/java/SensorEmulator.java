import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.Random;

public class SensorEmulator {

    public static void main(String[] args) {

        String sensorId = "test";

        if (args.length > 0) {
            sensorId = args[0];
        }

        System.out.println(sensorId);

        Random random = new Random();

        NormalDistribution distribution = new NormalDistribution(random.nextFloat() * 10 + 14, 3);

        int targetDir = -1;

        int counter = 0;

        Float temperature = random.nextFloat() * 10 + 14;
        while (true) {


            temperature += targetDir * (float)distribution.sample() / 72;

            try {
                System.out.printf("Sending temperature %f \n", temperature);
                Unirest.post("http://localhost:8182/sensors/data")
                        .queryString("id", sensorId)
                        .queryString("type", "temperature")
                        .queryString("value", temperature)
                        .asJson();
            } catch (UnirestException e) {
                System.out.println("Something wrong happened");
            }

            try {
                Thread.sleep(random.nextInt(1000) + 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (counter % 15 == 0) {
                targetDir *= -1;
            }

            counter++;
        }
    }
}
