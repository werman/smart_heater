package com.werman.heater.mobile.app;

public class Preferences {

    public final static String NAME = "private_prefs";

    public final static String TOKEN = "token";

    public final static String SERVER_URL = "server_url";
}
