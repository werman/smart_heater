package com.werman.heater.mobile.app.tasks.responses.sensor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MultiplySensorsStatisticInPeriod {

    private Map<Integer, SensorStatisticInPeriod> statistic;

    public Map<Integer, SensorStatisticInPeriod> getStatistic() {
        return statistic;
    }

    public void setStatistic(Map<Integer, SensorStatisticInPeriod> statistic) {
        this.statistic = statistic;
    }
}
