package com.werman.heater.mobile.app.tasks.responses.sensor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SensorStatisticInPeriod {

    private Integer granularity;

    private Date startDate;

    private Date endDate;

    private Integer measuresCount;

    private List<SensorStatisticFrame> data;

    public Integer getGranularity() {
        return granularity;
    }

    public void setGranularity(Integer granularity) {
        this.granularity = granularity;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getMeasuresCount() {
        return measuresCount;
    }

    public void setMeasuresCount(Integer measuresCount) {
        this.measuresCount = measuresCount;
    }

    public List<SensorStatisticFrame> getData() {
        return data;
    }

    public void setData(List<SensorStatisticFrame> data) {
        this.data = data;
    }
}
