package com.werman.heater.mobile.app.dialogs;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import java.util.Calendar;
import java.util.Date;

public class DatePicker  extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private Date initialDate;
    private DatePickListener datePickListener;

    public DatePicker(Date initialDate, DatePickListener listener) {
        this.initialDate = initialDate;
        datePickListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        c.setTime(initialDate);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int i, int i1, int i2) {
        final Calendar c = Calendar.getInstance();
        c.setTime(initialDate);
        c.set(Calendar.YEAR, i);
        c.set(Calendar.MONTH, i1);
        c.set(Calendar.DAY_OF_MONTH, i2);


        DialogFragment newFragment = new TimePicker(c.getTime(), datePickListener);
        newFragment.show(getFragmentManager(), "datePicker");
    }
}
