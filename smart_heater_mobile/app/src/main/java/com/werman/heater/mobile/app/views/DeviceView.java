package com.werman.heater.mobile.app.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.TextView;
import com.werman.heater.mobile.app.R;
import com.werman.heater.mobile.app.tasks.responses.InitResponse;

public class DeviceView extends CardView {

    private InitResponse.DeviceInfo deviceInfo;

    private TextView deviceNameText;
    private TextView deviceValueText;
    private TextView deviceStateText;

    public DeviceView(Context context) {
        this(context, null);
    }

    public DeviceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DeviceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        inflate(getContext(), R.layout.view_device, this);

        deviceNameText = (TextView) findViewById(R.id.text_device_name);
        deviceValueText = (TextView) findViewById(R.id.text_device_value);
        deviceStateText = (TextView) findViewById(R.id.text_device_state);
    }

    public void setDeviceInfo(InitResponse.DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
        deviceNameText.setText(deviceInfo.getName());
        deviceValueText.setText(deviceInfo.getTarget().toString());
        deviceStateText.setText(deviceInfo.getState());
    }
}
