package com.werman.heater.mobile.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.werman.heater.mobile.app.Preferences;
import com.werman.heater.mobile.app.R;
import com.werman.heater.mobile.app.tasks.responses.InitResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class EditNameDialog extends DialogFragment {

    public interface RenameSuccessListener {
        void onRenameSuccess();
    }

    private InitResponse.HouseInfo houseInfo;
    private InitResponse.SensorInfo sensorInfo;

    private EditText editName;
    private TextView currentNameText;
    private ProgressBar progressBar;

    private SharedPreferences sharedPref;

    private RenameSuccessListener renameSuccessListener;

    private AsyncTask<Void, Void, ?> asyncTask;

    private String currentName;

    private Button positiveButton;
    private Button negativeButton;

    public EditNameDialog(InitResponse.HouseInfo houseInfo) {
        super();
        this.houseInfo = houseInfo;
        asyncTask = new ChangeHouseNameTask();
        currentName = houseInfo.getName();
    }

    public EditNameDialog(InitResponse.HouseInfo houseInfo, InitResponse.SensorInfo sensorInfo) {
        super();
        this.houseInfo = houseInfo;
        this.sensorInfo = sensorInfo;
        asyncTask = new ChangeSensorNameTask();
        currentName = sensorInfo.getName();
    }

    public void setRenameSuccessListener(RenameSuccessListener renameSuccessListener) {
        this.renameSuccessListener = renameSuccessListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        sharedPref = getActivity().getSharedPreferences(Preferences.NAME, Context.MODE_PRIVATE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_change_name, null);

        editName = (EditText) view.findViewById(R.id.edit_new_name);

        currentNameText = (TextView) view.findViewById(R.id.text_current_name);
        currentNameText.setText(currentName);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_change_name);

        builder.setView(view)
                .setPositiveButton(R.string.btn_change_name_ok, null)
                .setNegativeButton(R.string.btn_change_name_cancel, null);

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        final AlertDialog dialog = (AlertDialog) getDialog();

        positiveButton = (Button) dialog.getButton(Dialog.BUTTON_POSITIVE);
        negativeButton = (Button) dialog.getButton(Dialog.BUTTON_NEGATIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                asyncTask.execute();
            }
        });

        stopWait();
    }

    private void showWait() {
        currentNameText.setVisibility(View.GONE);
        editName.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        positiveButton.setEnabled(false);
        negativeButton.setEnabled(false);
    }

    private void stopWait() {
        currentNameText.setVisibility(View.VISIBLE);
        editName.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        positiveButton.setEnabled(true);
        negativeButton.setEnabled(true);
    }

    private class ChangeHouseNameTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showWait();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new FormHttpMessageConverter());

                MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                map.add("houseId", houseInfo.getId());
                map.add("newName", editName.getText().toString());

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                headers.add("X-AUTH-TOKEN", sharedPref.getString(Preferences.TOKEN, ""));

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

                String serverUrl = sharedPref.getString(Preferences.SERVER_URL, "");

                HttpEntity<String> response = restTemplate.exchange(serverUrl + "/api/house/manage/change_name",
                                                                           HttpMethod.POST, request,
                                                                           String.class);
                return response.getBody();
            } catch (Exception e) {
                stopWait();
                Log.e("EditName", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            EditNameDialog.this.getDialog().cancel();
            renameSuccessListener.onRenameSuccess();
        }
    }

    private class ChangeSensorNameTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showWait();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new FormHttpMessageConverter());

                MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                map.add("houseId", houseInfo.getId());
                map.add("sensorId", sensorInfo.getId().toString());
                map.add("newName", editName.getText().toString());

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                headers.add("X-AUTH-TOKEN", sharedPref.getString(Preferences.TOKEN, ""));

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

                String serverUrl = sharedPref.getString(Preferences.SERVER_URL, "");

                HttpEntity<String> response = restTemplate.exchange(serverUrl + "/api/house/sensors/change_name",
                                                                           HttpMethod.POST, request,
                                                                           String.class);
                return true;
            } catch (Exception e) {
                Log.e("EditName", e.getMessage(), e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (!success) {
                stopWait();
            }
            EditNameDialog.this.getDialog().cancel();
            renameSuccessListener.onRenameSuccess();
        }
    }
}
