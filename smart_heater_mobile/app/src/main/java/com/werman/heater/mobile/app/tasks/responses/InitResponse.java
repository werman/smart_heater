package com.werman.heater.mobile.app.tasks.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InitResponse {

    @JsonProperty("houses")
    private List<HouseInfo> houses;

    public List<HouseInfo> getHouses() {
        return houses;
    }

    public static class HouseInfo {
        @JsonProperty("id")
        private String id;

        @JsonProperty("name")
        private String name;

        @JsonProperty("sensors")
        private List<SensorInfo> sensors;

        @JsonProperty("devices")
        private List<DeviceInfo> devices;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public List<SensorInfo> getSensors() {
            return sensors;
        }

        public List<DeviceInfo> getDevices() {
            return devices;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DeviceInfo {

        @JsonProperty("id")
        private String id;

        @JsonProperty("name")
        private String name;

        @JsonProperty("state")
        private String state;

        @JsonProperty("target")
        private Float target;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getState() {
            return state;
        }

        public Float getTarget() {
            return target;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SensorInfo implements Serializable {

        @JsonProperty("id")
        private Integer id;

        @JsonProperty("name")
        private String name;

        @JsonProperty("last_measured_value")
        private Float lastMeasuredValue;

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public Float getLastMeasuredValue() {
            return lastMeasuredValue;
        }
    }
}
