package com.werman.heater.mobile.app.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.werman.heater.mobile.app.R;
import com.werman.heater.mobile.app.tasks.responses.InitResponse;

public class SensorView extends CardView {

    public interface DetailsListener {
        void onShowDetails(InitResponse.SensorInfo sensorInfo);
    }

    public interface RenameListener {
        void onRename();
    }

    private InitResponse.SensorInfo sensorInfo;

    private TextView sensorNameText;
    private TextView sensorValueText;
    private View detailsView;

    public SensorView(Context context) {
        this(context, null);
    }

    public SensorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SensorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        inflate(getContext(), R.layout.view_sensor, this);

        sensorNameText = (TextView) findViewById(R.id.text_sensor_name);
        sensorValueText = (TextView) findViewById(R.id.text_sensor_value);
        detailsView = (View) findViewById(R.id.text_sensor_view_details);
    }

    public void setSensorInfo(InitResponse.SensorInfo sensorInfo) {
        this.sensorInfo = sensorInfo;
        sensorNameText.setText(sensorInfo.getName());
        if (sensorInfo.getLastMeasuredValue() != null) {
            sensorValueText.setText(sensorInfo.getLastMeasuredValue().toString());
        }
    }

    public void setLastValue(String lastValue) {
        sensorValueText.setText(lastValue);
    }

    public void setOnShowDetailsListener(final DetailsListener detailsListener) {
        detailsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailsListener.onShowDetails(sensorInfo);
            }
        });
    }

    public void setOnRenameListener(final RenameListener renameListener) {
        sensorNameText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                renameListener.onRename();
            }
        });
    }
}
