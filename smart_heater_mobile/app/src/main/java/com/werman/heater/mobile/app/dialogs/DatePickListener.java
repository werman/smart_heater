package com.werman.heater.mobile.app.dialogs;

import java.util.Date;

public interface DatePickListener {
    void onDatePicked(Date date);
}
