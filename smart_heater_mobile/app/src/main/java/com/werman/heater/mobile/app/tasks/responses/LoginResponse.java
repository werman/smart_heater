package com.werman.heater.mobile.app.tasks.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {

    @JsonProperty("token")
    private String token;

    @JsonProperty("error")
    private String error;

    @JsonProperty("name")
    private String name;

    public String getToken() {
        return token;
    }

    public String getError() {
        return error;
    }

    public String getName() {
        return name;
    }
}
