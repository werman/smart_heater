package com.werman.heater.mobile.app.dialogs;


import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;

import java.util.Calendar;
import java.util.Date;

public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private Date initialDate;
    private DatePickListener datePickListener;

    public TimePicker(Date initialDate, DatePickListener listener) {
        this.initialDate = initialDate;
        datePickListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        c.setTime(initialDate);
        int hour = c.get(Calendar.HOUR);
        int min = c.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, min, true);
    }

    @Override
    public void onTimeSet(android.widget.TimePicker timePicker, int i, int i1) {
        Calendar c = Calendar.getInstance();
        c.setTime(initialDate);
        c.set(Calendar.HOUR_OF_DAY, i);
        c.set(Calendar.MINUTE, i1);
        datePickListener.onDatePicked(c.getTime());
    }
}
