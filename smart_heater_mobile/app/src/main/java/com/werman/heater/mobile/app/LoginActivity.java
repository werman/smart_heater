package com.werman.heater.mobile.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.werman.heater.mobile.app.tasks.responses.LoginResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


public class LoginActivity extends ActionBarActivity implements View.OnClickListener {

    private TextView textError;
    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        buttonLogin = (Button) findViewById(R.id.button_login);
        buttonLogin.setOnClickListener(this);

        textError = (TextView) findViewById(R.id.text_login_error);
    }

    @Override
    public void onClick(View view) {
        EditText editServerUrl = (EditText) findViewById(R.id.edit_server_url);
        EditText editLogin = (EditText) findViewById(R.id.edit_login);
        EditText editPassword = (EditText) findViewById(R.id.edit_password);

        String serverUrl = editServerUrl.getText().toString();
        String login = editLogin.getText().toString();
        String password = editPassword.getText().toString();

        hideError();

        if (login.isEmpty() || password.isEmpty()) {
            showError("Login or password shouldn't be empty");
            return;
        }

        buttonLogin.setEnabled(false);

        LoginTask loginTask = new LoginTask();
        loginTask.setLogin(login);
        loginTask.setPassword(password);
        loginTask.setServerUrl(serverUrl);
        loginTask.execute();
    }

    private void showError(String error) {
        textError.setText(error);
        textError.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        textError.setVisibility(View.GONE);
    }

    private class LoginTask extends AsyncTask<Void, Void, LoginResponse> {

        private String login;
        private String password;
        private String serverUrl;

        @Override
        protected LoginResponse doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new FormHttpMessageConverter());

                MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
                map.add("login", login);
                map.add("password", password);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

                return restTemplate.postForObject(serverUrl + "/api/login", request, LoginResponse.class);
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(LoginResponse loginResponse) {

            buttonLogin.setEnabled(true);

            if (loginResponse != null) {
               if (loginResponse.getToken() != null) {

                   SharedPreferences sharedPref = getSharedPreferences(Preferences.NAME, Context.MODE_PRIVATE);
                   SharedPreferences.Editor editor = sharedPref.edit();

                   ((MyApplication)getApplication()).getRuntimeParams().setServerUrl(serverUrl);

                   editor.putString(Preferences.TOKEN, loginResponse.getToken());
                   editor.putString(Preferences.SERVER_URL, serverUrl);
                   editor.apply();

                   Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                   startActivity(intent);
               } else {
                   showError(loginResponse.getError());
               }
            } else {
                showError("Unable to reach server.");
            }
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public void setServerUrl(String serverUrl) {
            this.serverUrl = serverUrl;
        }
    }
}
