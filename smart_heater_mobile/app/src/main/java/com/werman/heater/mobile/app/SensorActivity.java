package com.werman.heater.mobile.app;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.werman.heater.mobile.app.dialogs.DatePickListener;
import com.werman.heater.mobile.app.dialogs.DatePicker;
import com.werman.heater.mobile.app.tasks.responses.InitResponse;
import com.werman.heater.mobile.app.tasks.responses.sensor.MultiplySensorsStatisticInPeriod;
import com.werman.heater.mobile.app.tasks.responses.sensor.SensorStatisticFrame;
import com.werman.heater.mobile.app.tasks.responses.sensor.SensorStatisticInPeriod;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class SensorActivity extends ActionBarActivity {

    private final int POINTS_COUNT = 8;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    private GraphView graph;
    private Button fromButton;
    private Button toButton;
    private Button refreshButton;
    private TextView fromText;
    private TextView toText;

    private Date startDate;
    private Date endDate;
    private Integer granularity = 30; // In seconds

    private LineGraphSeries<DataPoint> graphPoints;

    private InitResponse.SensorInfo sensorInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        sensorInfo = (InitResponse.SensorInfo) getIntent().getExtras().get("info");

        ((TextView) findViewById(R.id.text_sensor_name)).setText(sensorInfo.getName());

        fromButton = (Button) findViewById(R.id.button_from);
        toButton = (Button) findViewById(R.id.button_to);
        refreshButton = (Button) findViewById(R.id.button_refresh);
        fromText = (TextView) findViewById(R.id.text_from_date);
        toText = (TextView) findViewById(R.id.text_to_date);

        graph = (GraphView) findViewById(R.id.sensor_graph);
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX)
                {
                    calendar.setTimeInMillis((long)value);
                    return dateFormat.format(calendar.getTime());
                }
                else {
                    return String.format("%.1f", value);
                }
            }
        });

        graphPoints = new LineGraphSeries<>();
        graph.addSeries(graphPoints);

        endDate = Calendar.getInstance().getTime();
        startDate = new Date(endDate.getTime() - POINTS_COUNT * (granularity * 1000));

        fromButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickStartDate();
            }
        });

        toButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickEndDate();
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshData();
            }
        });

        new GraphTask().execute();
    }

    private void pickStartDate() {
        Date now = Calendar.getInstance().getTime();
        DialogFragment newFragment = new DatePicker(now, new DatePickListener() {
            @Override
            public void onDatePicked(Date date) {
                startDate = date;
                fromText.setText(dateFormat.format(date));
            }
        });
        newFragment.show(getFragmentManager(), "datePicker");
    }

    private void pickEndDate() {
        Date now = Calendar.getInstance().getTime();
        DialogFragment newFragment = new DatePicker(now, new DatePickListener() {
            @Override
            public void onDatePicked(Date date) {
                endDate = date;
                toText.setText(dateFormat.format(date));
            }
        });
        newFragment.show(getFragmentManager(), "datePicker");
    }

    private void refreshData() {
        granularity = (int)((endDate.getTime() - startDate.getTime()) / 8 / 1000);
        new GraphTask().execute();
    }

    private void displayData(MultiplySensorsStatisticInPeriod data) {
        SensorStatisticInPeriod statistic = data.getStatistic().get(sensorInfo.getId());

        List<DataPoint> points = new ArrayList<>();

        for (int i = 0; i < statistic.getData().size(); i++) {
            SensorStatisticFrame frame = statistic.getData().get(i);
            if (frame.getMeasuresCount() != 0) {
                points.add(new DataPoint(frame.getDate(), frame.getValue()));
            }
        }



        if (points.size() > 0) {
            graphPoints.resetData(points.toArray(new DataPoint[points.size()]));

            graph.getViewport().setMinX(startDate.getTime() - granularity * 1000 * 1.5);
            graph.getViewport().setMaxX(endDate.getTime() + granularity * 1000 * 1.5);
            graph.getViewport().setXAxisBoundsManual(true);
        }
    }

    private class GraphTask extends AsyncTask<Void, Void, MultiplySensorsStatisticInPeriod> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected MultiplySensorsStatisticInPeriod doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new FormHttpMessageConverter());

                MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

                SharedPreferences sharedPref = getSharedPreferences(Preferences.NAME, Context.MODE_PRIVATE);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                headers.add("X-AUTH-TOKEN", sharedPref.getString(Preferences.TOKEN, ""));

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

                String serverUrl = ((MyApplication) getApplication()).getRuntimeParams().getServerUrl();

                UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverUrl)
                                                       .path("/api/house/sensors/")
                                                       .queryParam("sensorUUIDs", sensorInfo.getId().toString())
                                                       .queryParam("granularityInSeconds", granularity.toString())
                                                       .queryParam("startDate", dateFormat.format(startDate))
                                                       .queryParam("endDate", dateFormat.format(endDate));

                HttpEntity<MultiplySensorsStatisticInPeriod> response = restTemplate.exchange(builder.build().encode().toUri(),
                                                                                                     HttpMethod.GET, request,
                                                                                                     MultiplySensorsStatisticInPeriod.class);
                return response.getBody();
            } catch (Exception e) {
                Log.e("SensorActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(MultiplySensorsStatisticInPeriod response) {
            if (response != null) {
                displayData(response);
            }
        }
    }
}
