package com.werman.heater.mobile.app;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class MyApplication extends Application {

    RuntimeParams runtimeParams = new RuntimeParams();

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferences sharedPref = getSharedPreferences(Preferences.NAME, Context.MODE_PRIVATE);

        if (sharedPref.contains(Preferences.SERVER_URL)) {
            runtimeParams.setServerUrl(sharedPref.getString(Preferences.SERVER_URL, ""));
        }
    }

    public RuntimeParams getRuntimeParams() {
        return runtimeParams;
    }
}
