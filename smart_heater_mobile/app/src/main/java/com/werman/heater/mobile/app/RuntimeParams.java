package com.werman.heater.mobile.app;

import com.werman.heater.mobile.app.tasks.responses.InitResponse;

public class RuntimeParams {

    private String serverUrl;

    private InitResponse initData;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public InitResponse getInitData() {
        return initData;
    }

    public void setInitData(InitResponse initData) {
        this.initData = initData;
    }
}
