package com.werman.heater.mobile.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.werman.heater.mobile.app.dialogs.EditNameDialog;
import com.werman.heater.mobile.app.tasks.responses.InitResponse;
import com.werman.heater.mobile.app.tasks.responses.sensor.LastSensorsValue;
import com.werman.heater.mobile.app.views.DeviceView;
import com.werman.heater.mobile.app.views.SensorView;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends ActionBarActivity {

    private ProgressBar progress;

    private ViewGroup retryView;

    private RecyclerView housesRecyclerView;
    private MyAdapter housesAdapter;
    private RecyclerView.LayoutManager housesLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        progress = (ProgressBar) findViewById(R.id.progress_data);
        retryView = (ViewGroup) findViewById(R.id.layout_data_error);

        Button retryButton = (Button) findViewById(R.id.button_data_retry);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new InitTask().execute();
            }
        });

        housesRecyclerView = (RecyclerView) findViewById(R.id.scroll_houses);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        housesRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        housesLayoutManager = new LinearLayoutManager(this);
        housesRecyclerView.setLayoutManager(housesLayoutManager);

        // specify an adapter (see also next example)
        housesAdapter = new MyAdapter(new ArrayList<InitResponse.HouseInfo>());
        housesRecyclerView.setAdapter(housesAdapter);

        if (((MyApplication) getApplication()).getRuntimeParams().getInitData() == null) {
            new InitTask().execute();
        } else {
            progress.setVisibility(View.GONE);
            showData();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showData() {
        InitResponse data = ((MyApplication) getApplication()).getRuntimeParams().getInitData();

        housesAdapter.setData(data.getHouses());
        housesAdapter.notifyDataSetChanged();
    }

    private void refreshCurrentValues() {



    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private List<InitResponse.HouseInfo> houses;

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView textHouseName;
            private LinearLayout layoutSensors;
            private LinearLayout layoutDevices;

            public ViewHolder(View v) {
                super(v);

                textHouseName = (TextView) v.findViewById(R.id.text_house_name);
                layoutSensors = (LinearLayout) v.findViewById(R.id.layout_house_sensors);
                layoutDevices = (LinearLayout) v.findViewById(R.id.layout_house_devices);
            }
        }

        public MyAdapter(List<InitResponse.HouseInfo> houses) {
            this.houses = houses;
        }

        public void setData(List<InitResponse.HouseInfo> houses) {
            this.houses = houses;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                             .inflate(R.layout.view_house, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final InitResponse.HouseInfo houseInfo = houses.get(position);
            if (houseInfo.getName() != null && !houseInfo.getName().isEmpty()) {
                holder.textHouseName.setText(houseInfo.getName());
            } else {
                holder.textHouseName.setText("No name");
            }

            holder.textHouseName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EditNameDialog editNameDialog = new EditNameDialog(houseInfo);
                    editNameDialog.setRenameSuccessListener(new EditNameDialog.RenameSuccessListener() {
                        @Override
                        public void onRenameSuccess() {
                            new InitTask().execute();
                        }
                    });
                    editNameDialog.show(getFragmentManager(), "EditHouseName");
                }
            });

            holder.layoutSensors.removeAllViews();
            for (final InitResponse.SensorInfo sensorInfo : houseInfo.getSensors()) {
                SensorView sensorView = new SensorView(holder.layoutSensors.getContext());
                sensorView.setSensorInfo(sensorInfo);

                sensorView.setOnShowDetailsListener(new SensorView.DetailsListener() {
                    @Override
                    public void onShowDetails(InitResponse.SensorInfo sensorInfo) {
                        Intent intent = new Intent(HomeActivity.this, SensorActivity.class);
                        Bundle b = new Bundle();
                        b.putSerializable("info", sensorInfo);
                        intent.putExtras(b);
                        startActivity(intent);
                    }
                });

                sensorView.setOnRenameListener(new SensorView.RenameListener() {
                    @Override
                    public void onRename() {
                        EditNameDialog editNameDialog = new EditNameDialog(houseInfo, sensorInfo);
                        editNameDialog.setRenameSuccessListener(new EditNameDialog.RenameSuccessListener() {
                            @Override
                            public void onRenameSuccess() {
                                new InitTask().execute();
                            }
                        });
                        editNameDialog.show(getFragmentManager(), "EditSensorName");
                    }
                });

                holder.layoutSensors.addView(sensorView);
            }

            for (InitResponse.DeviceInfo deviceInfo : houseInfo.getDevices()) {
                DeviceView deviceView = new DeviceView(holder.layoutSensors.getContext());
                deviceView.setDeviceInfo(deviceInfo);
                holder.layoutDevices.addView(deviceView);
            }
        }

        @Override
        public int getItemCount() {
            return houses.size();
        }
    }

    private class RefreshValuesTask extends AsyncTask<Void, Void, LastSensorsValue[]> {

        @Override
        protected LastSensorsValue[] doInBackground(Void... voids) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new FormHttpMessageConverter());

                MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

                InitResponse initDate = ((MyApplication) getApplication()).getRuntimeParams().getInitData();

                SharedPreferences sharedPref = getSharedPreferences(Preferences.NAME, Context.MODE_PRIVATE);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                headers.add("X-AUTH-TOKEN", sharedPref.getString(Preferences.TOKEN, ""));

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

                String serverUrl = ((MyApplication) getApplication()).getRuntimeParams().getServerUrl();

                HttpEntity<LastSensorsValue[]> response = restTemplate.exchange(serverUrl + "/api/house/sensors/last_value",
                                                                                 HttpMethod.GET, request,
                                                                                 LastSensorsValue[].class);
                return response.getBody();
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }
    }

    private class InitTask extends AsyncTask<Void, Void, InitResponse> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            retryView.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected InitResponse doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new FormHttpMessageConverter());

                MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

                SharedPreferences sharedPref = getSharedPreferences(Preferences.NAME, Context.MODE_PRIVATE);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                headers.add("X-AUTH-TOKEN", sharedPref.getString(Preferences.TOKEN, ""));

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

                String serverUrl = ((MyApplication) getApplication()).getRuntimeParams().getServerUrl();

                HttpEntity<InitResponse> response = restTemplate.exchange(serverUrl + "/api/init",
                                                                                 HttpMethod.GET, request,
                                                                                 InitResponse.class);
                return response.getBody();
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(InitResponse response) {
            progress.setVisibility(View.GONE);

            if (response != null) {
                ((MyApplication) getApplication()).getRuntimeParams().setInitData(response);
                showData();
            } else {
                retryView.setVisibility(View.VISIBLE);
            }
        }
    }
}
