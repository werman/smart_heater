function sprintf() {
    var args = arguments,
        string = args[0],
        i = 1;
    return string.replace(/%((%)|s|d)/g, function (m) {
        // m is the matched format, e.g. %s, %d
        var val = null;
        if (m[2]) {
            val = m[2];
        } else {
            val = args[i];
            // A switch statement so that the formatter can be extended. Default is %s
            switch (m) {
                case '%d':
                    val = parseFloat(val);
                    if (isNaN(val)) {
                        val = 0;
                    }
                    break;
            }
            i++;
        }
        return val;
    });
}

var AUTH_HEADER = "X-AUTH-TOKEN";
var STORAGE_NAME = "name";

var AUTH_TOKEN = localStorage.getItem(AUTH_HEADER);

var URL = "http://109.86.206.240:8080/api";

var API_HEADERS = {
    'Accept': 'application/json'
};

var sensorIds = [];

if (AUTH_HEADER in localStorage) {
    API_HEADERS[AUTH_HEADER] = AUTH_TOKEN;
    $("#menu-signin").hide();
    $("#menu-signup").hide();
    $("#menu-signout").show();
    $("#menu-name").text(localStorage.getItem(STORAGE_NAME));
} else {
    $("#menu-signin").show();
    $("#menu-signup").show();
    $("#menu-signout").hide();
}

$(document).ready(function () {
    if (typeof google !== 'undefined') {
        google.load('visualization', '1', {
            packages: ['corechart', 'line'],
            callback: function () {
                // do stuff, if you wan't - it doesn't matter, because the page isn't blank!
            }
        });
    }
});

function apiPost(request_url, request_data, success_callback, error_callback) {
    $.ajax({
        url: URL + request_url,
        headers: API_HEADERS,
        type: 'POST',
        crossDomain: true,
        data: request_data,
        success: success_callback,
        error: error_callback
    });
}

function apiGet(request_url, request_data, success_callback, error_callback) {
    $.ajax({
        url: URL + request_url,
        headers: API_HEADERS,
        type: 'GET',
        crossDomain: true,
        data: request_data,
        success: success_callback,
        error: error_callback
    });
}


function registerNewUser() {
    var formElement = document.getElementById("signup-form");
    var formData = serialize(formElement);

    apiPost('/register', formData,
        function (res) {
            if (res["token"] != null) {
                localStorage.setItem(AUTH_HEADER, res["token"]);
                localStorage.setItem(STORAGE_NAME, res["name"]);
                window.location.href = '/home';
            }
        },
        function (res) {
            if (res["error"] != null) {
                $("#error").text(res["error"]);
            } else {
                $("#error").text("Error!");
            }
        });
}

function logIn() {
    var formElement = document.getElementById("login-form");
    var formData = serialize(formElement);

    apiPost('/login', formData,
        function (res) {
            if (res["token"] != null) {
                localStorage.setItem(AUTH_HEADER, res["token"]);
                localStorage.setItem(STORAGE_NAME, res["name"]);
                window.location.href = '/home';
            }
        },
        function (res) {
            if (res["error"] != null) {
                $("#error").text(res["error"]);
            } else {
                $("#error").text("Error!");
            }
        });
}

function logOut() {
    localStorage.removeItem(AUTH_HEADER);
    localStorage.removeItem(STORAGE_NAME);
    document.location.href = "/";
}

function showGeneralInfo() {

    var houseTemplate = Handlebars.compile($("#house-template").html());
    var houseInfoTemplate = Handlebars.compile($("#house-info-template").html());
    var sensorTemplate = Handlebars.compile($("#sensor-info-template").html());
    var deviceTemplate = Handlebars.compile($("#device-info-template").html());

    apiGet("/init", {}, function (res) {
        var houses = res["houses"];

        sensorIds = [];

        if (houses.length > 0) {

            $("#info").empty();

            for (var houseId in houses) {

                var house = houses[houseId];

                var houseTemplateParams = {};

                house["sensorsCount"] = house["sensors"].length;
                house["devicesCount"] = house["devices"].length;

                houseTemplateParams["houseInfo"] = houseInfoTemplate(house);
                houseTemplateParams["sensors"] = "";
                houseTemplateParams["devices"] = "";

                var sensors = house["sensors"];

                for (var sensorId in sensors) {
                    var sensor = sensors[sensorId];
                    sensorIds.push(sensor["id"]);
                    sensor["houseId"] = house["id"];
                    houseTemplateParams["sensors"] += sensorTemplate(sensor);
                }

                var devices = house["devices"];

                for (var deviceId in devices) {
                    var device = devices[deviceId];
                    device["houseId"] = house["id"];
                    houseTemplateParams["devices"] += deviceTemplate(device);
                }

                var houseDiv = houseTemplate(houseTemplateParams);

                $("#info").append(houseDiv);

                retrieveInitSensorsHistory(house["sensors"]);
                retrieveSensorsCurrentValues();

                window.setInterval(function(){
                    retrieveSensorsCurrentValues();
                }, 5000);
            }
        }
    });
}

function retrieveInitSensorsHistory(sensors) {
    var sensorsId = [];
    for (var sensorId in sensors) {
        sensorsId.push(sensors[sensorId]["id"]);
    }

    var formattedNow = moment().format("DD-MM-YYYY HH:mm");

    var formattedStart = moment().subtract(10, "minutes").format("DD-MM-YYYY HH:mm");

    getSensorsHistory(sensorsId, formattedStart, formattedNow, 10);
}

function retrieveSensorsCurrentValues() {
    apiGet('/house/sensors/last_value', {
            "sensorsId": sensorIds.join(",")
        },
        function (res) {
            for (var id in res) {
                var sensor = res[id];
                $("#sensor-value-" + sensor["id"]).text(sensor["value"].toFixed(1).toString());
            }
        },
        function (res) {

        });
}

function getSensorsHistory(sensors, startDate, endDate, granularity) {
    apiGet("/house/sensors/",
        {
            "sensorUUIDs": sensors.join(","),
            "startDate": startDate,
            "endDate": endDate,
            "granularityInSeconds": granularity
        }, function (res) {
            var statistic = res["statistic"];

            for (var sensorId in statistic) {
                if (statistic.hasOwnProperty(sensorId)) {
                    var data = new google.visualization.DataTable();


                    data.addColumn('datetime', 'Time');
                    data.addColumn('number', 'Value');

                    var sensorData = statistic[sensorId]["data"];

                    for (var frameId in sensorData) {
                        var frame = sensorData[frameId];
                        if (frame["measuresCount"] > 0) {
                            data.addRow([new Date(frame["date"]), frame["value"] ]);
                        }
                    }

                    var options = {
                        width: 600,
                        height: 300,
                        hAxis: {
                            title: 'Time',
                            format: 'd/M HH:mm'
                        },
                        vAxis: {
                            title: 'Temperature'
                        }
                    };

                    var chart = new google.visualization.LineChart(document.getElementById('chart-' + sensorId));
                    chart.draw(data, options);
                }
            }
        },
        function (res) {

        });
}

var currentEditHouseId;
var currentEditSensorId;
var currentEditDeviceId;

function sendChangeHouseName() {
    var houseId = currentEditHouseId;
    var formElement = document.getElementById("house-name-form");
    var newName = formElement["newName"].value;
    var formData = serialize(formElement);
    formData += "&houseId=" + houseId;
    apiPost('/house/name', formData,
        function (res) {
            $("#dialog-house-name").hide();
            $("#house-name-" + houseId).text(newName);
        },
        function (res) {
            $("#dialog-house-name").hide();
        });
}

function sendChangeSensorName() {
    var sensorId = currentEditSensorId;
    var formElement = document.getElementById("sensor-name-form");
    var newName = formElement["newName"].value;
    var formData = serialize(formElement);
    formData += "&houseId=" + currentEditHouseId;
    formData += "&sensorId=" + sensorId;
    apiPost('/house/sensors/name', formData,
        function (res) {
            $("#dialog-sensor-name").hide();
            $("#sensor-name-" + sensorId).text(newName);
        },
        function (res) {
            $("#dialog-sensor-name").hide();
        });
}

function sendChangeDeviceName() {
    var deviceId = currentEditDeviceId;
    var formElement = document.getElementById("device-name-form");
    var newName = formElement["newName"].value;
    var formData = serialize(formElement);
    formData += "&houseId=" + currentEditHouseId;
    formData += "&deviceId=" + deviceId;
    apiPost('/house/devices/name', formData,
        function (res) {
            $("#dialog-device-name").hide();
            $("#device-name-" + deviceId).text(newName);
        },
        function (res) {
            $("#dialog-device-name").hide();
        });
}


function showEditHouseName(houseId) {
    currentEditHouseId = houseId;
    $("#house-name").text("");
    $("#dialog-house-name").show();
}

function showEditSensorName(sensorId, houseId) {
    currentEditSensorId = sensorId;
    currentEditHouseId = houseId;
    $("#sensor-name").text("");
    $("#dialog-sensor-name").show();
}

function showEditDeviceName(deviceId, houseId) {
    currentEditDeviceId = deviceId;
    currentEditHouseId = houseId;
    $("#device-name").text("");
    $("#dialog-device-name").show();
}

function enableDevice(deviceId, houseId) {
    var formData = "";
    formData += "&houseId=" + houseId;
    formData += "&deviceId=" + deviceId;
    formData += "&on=true";
    apiPost('/house/devices/power', formData,
        function (res) {
            $("#device-state-" + deviceId).text("on");
        },
        function (res) {

        });
}

function disableDevice(deviceId, houseId) {
    var formData = "";
    formData += "&houseId=" + houseId;
    formData += "&deviceId=" + deviceId;
    formData += "&on=false";
    apiPost('/house/devices/power', formData,
        function (res) {
            $("#device-state-" + deviceId).text("off");
        },
        function (res) {

        });
}

function showEditTargetValueDialog(deviceId, houseId) {
    currentEditDeviceId = deviceId;
    currentEditHouseId = houseId;
    $("#device-target").text("");
    $("#dialog-device-target").show();
}

function sendChangeDeviceTarget() {
    var deviceId = currentEditDeviceId;
    var formElement = document.getElementById("device-target-form");
    var targetValue = formElement["targetValue"].value;
    var formData = "";
    formData += "&houseId=" + currentEditHouseId;
    formData += "&deviceId=" + deviceId;
    formData += "&targetValue=" + targetValue;
    apiPost('/house/devices/target_value', formData,
        function (res) {
            $("#dialog-device-target").hide();
            $("#device-target-" + deviceId).text(targetValue);
        },
        function (res) {
            $("#dialog-device-target").hide();
        });
}

function showChangeTargetDeviceValue(deviceId, houseId) {
    currentEditDeviceId = deviceId;
    currentEditHouseId = houseId;
    $("#device-target").text("");
    $("#dialog-device-target").show();
}

function setStartDate(sensorId) {

}

function setEndDate(sensorId) {

}

function showGraphic(sensorId) {

}