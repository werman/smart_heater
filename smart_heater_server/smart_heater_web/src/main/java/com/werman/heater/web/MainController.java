package com.werman.heater.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @RequestMapping(value = {"/", "index"}, method = RequestMethod.GET)
    public String indexPage() {
        return "public/views/index";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signupPage() {
        return "public/views/signup";
    }

    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public String signinPage() {
        return "public/views/signin";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String homePage() {
        return "public/views/home";
    }
}
