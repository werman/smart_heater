package com.werman.heater.db.utils;

import org.jooq.util.DefaultGeneratorStrategy;
import org.jooq.util.Definition;
import org.jooq.util.TableDefinition;

public class ClassesCreateStrategy extends DefaultGeneratorStrategy {

    @Override
    public String getJavaClassName(Definition definition, Mode mode) {

        String name = "DB" + super.getJavaClassName(definition, mode);

        if (mode == Mode.DEFAULT && definition instanceof TableDefinition) {
            name += "Table";
        }

        return name;
    }

}
