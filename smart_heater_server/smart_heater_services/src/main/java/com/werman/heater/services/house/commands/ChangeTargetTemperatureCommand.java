package com.werman.heater.services.house.commands;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChangeTargetTemperatureCommand extends RemoteHouseCommand {

    @JsonProperty("sensor_id")
    private Integer sensorId;
    @JsonProperty("target_temperature")
    private Float targetTemperature;

    private ChangeTargetTemperatureCommand() {
        super();
    }

    public static RemoteHouseCommand create(Integer sensorId, Float targetTemperature) {
        RemoteHouseCommand command = new ChangeTargetTemperatureCommand();
        command.setMethodName("change_target_temperature");
        return command;
    }
}
