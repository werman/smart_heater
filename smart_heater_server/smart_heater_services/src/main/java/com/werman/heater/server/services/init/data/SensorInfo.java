package com.werman.heater.server.services.init.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class SensorInfo {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("last_measure_time")
    private Date lastMeasureTime;

    @JsonProperty("last_measured_value")
    private Float lastMeasuredValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastMeasureTime() {
        return lastMeasureTime;
    }

    public void setLastMeasureTime(Date lastMeasureTime) {
        this.lastMeasureTime = lastMeasureTime;
    }

    public Float getLastMeasuredValue() {
        return lastMeasuredValue;
    }

    public void setLastMeasuredValue(Float lastMeasuredValue) {
        this.lastMeasuredValue = lastMeasuredValue;
    }
}
