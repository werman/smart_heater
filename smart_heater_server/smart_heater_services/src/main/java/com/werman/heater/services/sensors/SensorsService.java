package com.werman.heater.services.sensors;

import com.werman.heater.db.dao.SensorDAO;
import com.werman.heater.db.dao.SensorHistoryDAO;
import com.werman.heater.db.data.tables.pojos.DBSensor;
import com.werman.heater.db.data.tables.pojos.DBSensorHistoryEntry;
import com.werman.heater.services.security.SecurityUserInfo;
import com.werman.heater.services.sensors.responses.LastSensorsValues;
import com.werman.heater.services.sensors.responses.MultiplySensorsStatisticInPeriod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SensorsService {

    @Autowired
    private SensorDAO sensorDAO;

    @Autowired
    private SensorHistoryDAO sensorHistoryDAO;

    public boolean addSensorValue(SecurityUserInfo securityUserInfo, Integer sensorId, Float value) {
        DBSensor sensor = sensorDAO.findById(sensorId);

        if (sensor != null) {
            if (securityUserInfo.ownHouse(sensor.getHouseId())) {
                Date now = Calendar.getInstance().getTime();
                DBSensorHistoryEntry sensorHistoryEntry = new DBSensorHistoryEntry();
                sensorHistoryEntry
                        .setSensorId(sensorId)
                        .setValue(value)
                        .setMeasureTime(new Timestamp(now.getTime()));

                sensorHistoryDAO.insert(sensorHistoryEntry);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public MultiplySensorsStatisticInPeriod getSensorsStatistic(SecurityUserInfo securityUserInfo,
                                                                List<Integer> sensorIds,
                                                                Date startDate,
                                                                Date endDate,
                                                                Integer granularityInSeconds) {

        return new MultiplySensorsStatisticInPeriod();
    }

        public LastSensorsValues getLastSensorsValues(SecurityUserInfo securityUserInfo, List<Integer> sensorIds) {
        Map<Integer, Float> lastValuesMap = sensorHistoryDAO.getLastSensorsValues(sensorIds);
        LastSensorsValues lastSensorsValues = new LastSensorsValues();
        lastSensorsValues.setValues(lastValuesMap);
        return lastSensorsValues;
    }
}
