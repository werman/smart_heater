package com.werman.heater.services.sensors.responses;

import java.util.Map;

public class MultiplySensorsStatisticInPeriod {

    private Map<Integer, SensorStatisticInPeriod> statistic;

    public Map<Integer, SensorStatisticInPeriod> getStatistic() {
        return statistic;
    }

    public void setStatistic(Map<Integer, SensorStatisticInPeriod> statistic) {
        this.statistic = statistic;
    }
}
