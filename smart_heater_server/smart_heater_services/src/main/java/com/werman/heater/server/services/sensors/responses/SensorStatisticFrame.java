package com.werman.heater.server.services.sensors.responses;

import java.util.Date;

public class SensorStatisticFrame {

    private Date date;

    private Float value = 0f;

    private Integer measuresCount = 0;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Integer getMeasuresCount() {
        return measuresCount;
    }

    public void setMeasuresCount(Integer measuresCount) {
        this.measuresCount = measuresCount;
    }
}
