package com.werman.heater.server.services.devices.commands;

public class PowerOnCommand extends RemoteDeviceCommand {

    private PowerOnCommand() {
        super();
    }

    public static RemoteDeviceCommand create(String deviceId) {
        PowerOnCommand command = new PowerOnCommand();
        command.setMethodName("power_on");
        command.addParam("id", deviceId);
        return command;
    }
}
