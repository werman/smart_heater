package com.werman.heater.server.services.devices.commands;

import com.fasterxml.jackson.annotation.JsonValue;

public enum RemoteCommandResult {
    RESULT_SUCCESS("Success"),
    RESULT_REMOTE_HOST_UNREACHABLE("Remote host is unreachable"),
    RESULT_ENDPOINT_NO_REGISTERED("Endpoint not registered"),
    RESULT_INTERNAL_ERROR("Internal server error"),
    RESULT_GENERIC_ERROR("Error");

    private String name;

    RemoteCommandResult(String name) {
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }
}
