package com.werman.heater.services.sensors;

import com.werman.heater.services.security.RestrictUnauthorized;
import com.werman.heater.services.security.SecurityUserInfo;
import com.werman.heater.services.security.UserAuth;
import com.werman.heater.services.security.restrictions.HouseOwnerRestriction;
import com.werman.heater.services.sensors.responses.MultiplySensorsStatisticInPeriod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController(value = "/api/house/sensors")
public class TemperatureController {

    @Autowired
    private SensorsService sensorsService;

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/temperature", method = RequestMethod.POST)
    public void addTemperatureInfo( @RequestParam Integer sensorId,
                                    @RequestParam Float value,
                                    @UserAuth SecurityUserInfo securityUserInfo) {
        boolean success = sensorsService.addSensorValue(securityUserInfo, sensorId, value);
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/temperature", method = RequestMethod.GET)
    public @ResponseBody
    MultiplySensorsStatisticInPeriod getTemperatureInfo( @RequestParam List<Integer> sensorIds,
                                    @RequestParam Date startDate,
                                    @RequestParam Date endDate,
                                    @RequestParam Integer granularityInSeconds,
                                    @UserAuth SecurityUserInfo securityUserInfo) {
        //TODO: validate date interval

        return sensorsService.getSensorsStatistic(securityUserInfo, sensorIds, startDate, endDate, granularityInSeconds);
    }
}
