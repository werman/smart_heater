package com.werman.heater.server.services.security.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginResponse {

    @JsonProperty("token")
    private String token;

    @JsonProperty("name")
    private String name;

    @JsonProperty("error")
    private String error;

    public LoginResponse(String token, String name, String error) {
        this.token = token;
        this.name = name;
        this.error = error;
    }
}
