package com.werman.heater.services.security.restrictions;

import com.werman.heater.services.security.SecurityUserInfo;

import javax.servlet.http.HttpServletRequest;

public interface Restriction {

    boolean hasAccess(HttpServletRequest httpRequest, SecurityUserInfo securityUserInfo);
}
