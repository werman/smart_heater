package com.werman.heater.server.rest;

import com.werman.heater.server.db.data.enums.DBSensorType;
import com.werman.heater.server.services.security.RestrictUnauthorized;
import com.werman.heater.server.services.security.SecurityUserInfo;
import com.werman.heater.server.services.security.UserAuth;
import com.werman.heater.server.services.security.restrictions.HouseOwnerRestriction;
import com.werman.heater.server.services.sensors.SensorsService;
import com.werman.heater.server.services.sensors.responses.RegisterSensorResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController("TemperatureController")
@RequestMapping(value = "/api/house/sensors/temperature")
public class TemperatureController {

    @Autowired
    private SensorsService sensorsService;

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public RegisterSensorResult addTemperatureSensor(@RequestParam String sensorUUID,
                                                     @RequestParam String houseId,
                                                     @UserAuth SecurityUserInfo securityUserInfo) {
        return sensorsService.registerSensor(sensorUUID, houseId, DBSensorType.temperature);
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/data", method = RequestMethod.POST)
    public void addTemperatureInfo(@RequestParam Integer sensorUUID,
                                   @RequestParam Float value,
                                   @UserAuth SecurityUserInfo securityUserInfo,
                                   HttpServletResponse response) {
        boolean success = sensorsService.addSensorValue(securityUserInfo, sensorUUID, value);
        if (!success) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }
    }
}
