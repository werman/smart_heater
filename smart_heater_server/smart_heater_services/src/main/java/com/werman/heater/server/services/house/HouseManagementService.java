package com.werman.heater.server.services.house;

import com.werman.heater.server.db.dao.HouseDAO;
import com.werman.heater.server.db.data.tables.pojos.DBHouse;
import com.werman.heater.server.services.security.SecurityUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HouseManagementService {

    @Autowired
    private HouseDAO houseDAO;

    public boolean registerHouseEndpoint(SecurityUserInfo securityUserInfo, String houseId, String url, String token) {
        if (!houseDAO.existsById(houseId)) {
            DBHouse dbHouseRecord = new DBHouse();
            dbHouseRecord.setId(houseId);
            dbHouseRecord.setServiceAddress(url);
            dbHouseRecord.setServiceToken(token);
            dbHouseRecord.setOwnerId(securityUserInfo.getId());
            dbHouseRecord.setName("Unnamed");
            houseDAO.insert(dbHouseRecord);
        } else {
            if (securityUserInfo.ownHouse(houseId)) {
                houseDAO.updateEndpoint(houseId, url, token);
            } else {
                Integer owner = houseDAO.getOwnerId(houseId);
                if (securityUserInfo.getId() == owner)
                {
                    houseDAO.updateEndpoint(houseId, url, token);
                }
                else {
                    return false;
                }
            }
        }
        return true;
    }

    public void changeName(String houseId, String newName) {
        houseDAO.updateName(houseId, newName);
    }
}
