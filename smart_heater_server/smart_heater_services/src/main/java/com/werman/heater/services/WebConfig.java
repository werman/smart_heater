package com.werman.heater.services;

import com.werman.heater.services.security.AuthorizedMethodInterceptor;
import com.werman.heater.services.security.RequestUserAuthInterceptor;
import com.werman.heater.services.security.UserAuthMethodArgumentResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public RequestUserAuthInterceptor requestUserAuthInterceptor() {
        return new RequestUserAuthInterceptor();
    }

    @Bean
    public AuthorizedMethodInterceptor authorizedMethodInterceptor() {
        return new AuthorizedMethodInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor( requestUserAuthInterceptor() );
        registry.addInterceptor( authorizedMethodInterceptor() );
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        UserAuthMethodArgumentResolver userAuthMethodArgumentResolver = new UserAuthMethodArgumentResolver();
        argumentResolvers.add(0, userAuthMethodArgumentResolver);
    }
}
