package com.werman.heater.services.security;

import com.werman.heater.db.data.tables.pojos.DBUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class LoginManager {

    @Value("${security.token.expire}")
    private Long EXPIRE_DELTA;

    @Autowired
    private UserService userService;

    @Autowired
    private CryptService cryptService;

    public SecurityUserInfo loginAndGetSecurityInfo(String login, String password) {
        DBUserDetails userDetails = userService.getUserDetails(login);

        if (userDetails != null) {

            String passwordHash = cryptService.getPasswordHash(password);

            if (userDetails.getPasswordHash().equals(passwordHash)) {
                long now = Calendar.getInstance().getTimeInMillis();
                SecurityUserInfo securityUserInfo = new SecurityUserInfo();
                securityUserInfo.setExpireDate(now + EXPIRE_DELTA);
                return securityUserInfo;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
