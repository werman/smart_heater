package com.werman.heater.server.db.dao;

import com.werman.heater.server.db.data.tables.DBSensorTable;
import com.werman.heater.server.db.data.tables.daos.DBSensorDao;
import com.werman.heater.server.db.data.tables.records.DBSensorRecord;
import org.jooq.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.jooq.impl.DSL.*;

@Service
public class SensorDAO extends DBSensorDao {

    private final DBSensorTable SENSOR = DBSensorTable.SENSOR;

    @Autowired
    public SensorDAO(Configuration configuration) {
        super(configuration);
    }

    public boolean isBelongToHouse(Integer sensorId, String houseId) {
        return using(configuration())
                       .fetchExists(selectOne()
                                            .from(SENSOR)
                                            .where(SENSOR.ID.eq(sensorId))
                                            .and(SENSOR.HOUSE_ID.eq(houseId)));
    }

    public boolean exists(String sensorUUID, String houseId) {
        return using(configuration())
                       .fetchExists(selectOne()
                                            .from(SENSOR)
                                            .where(SENSOR.IDENTIFIER.eq(sensorUUID))
                                            .and(SENSOR.HOUSE_ID.eq(houseId)));
    }

    public Integer getSensorId(String sensorUUID, String houseId) {
        return using(configuration())
                       .fetchOne(select(SENSOR.ID)
                                         .from(SENSOR)
                                         .where(SENSOR.IDENTIFIER.eq(sensorUUID))
                                         .and(SENSOR.HOUSE_ID.eq(houseId)))
                       .into(Integer.class);
    }

    public Integer insertAndGetId(DBSensorRecord record) {
        DBSensorRecord result = using(configuration())
                                        .insertInto(SENSOR)
                                        .set(record)
                                        .returning(SENSOR.ID)
                                        .fetchOne();
        return result.getId();
    }

    public List<DBSensorRecord> getSensorsMinimalInfoByHouseId(String houseId) {
        return using(configuration())
                       .select(SENSOR.ID, SENSOR.NAME, SENSOR.LAST_MEASURE_TIME, SENSOR.LAST_VALUE)
                       .from(SENSOR)
                       .where(SENSOR.HOUSE_ID.eq(houseId))
                       .fetchInto(DBSensorRecord.class);
    }

    public List<DBSensorRecord> getLastSensorsValues(List<Integer> ids) {
        return using(configuration()).select(SENSOR.ID, SENSOR.LAST_VALUE, SENSOR.LAST_MEASURE_TIME)
                       .from(SENSOR)
                       .where(SENSOR.ID.in(ids))
                       .fetchInto(DBSensorRecord.class);
    }

    public void updateSensorName(String houseId, Integer sensorId, String newName) {
        using(configuration())
                .update(SENSOR)
                .set(SENSOR.NAME, newName)
                .where(SENSOR.HOUSE_ID.eq(houseId))
                .and(SENSOR.ID.eq(sensorId))
                .execute();
    }

    public void updateLastMeasure(String houseId, Integer sensorId, Float value, Date lastMeasure) {
        using(configuration())
                .update(SENSOR)
                .set(SENSOR.LAST_MEASURE_TIME, new Timestamp(lastMeasure.getTime()))
                .set(SENSOR.LAST_VALUE, value)
                .where(SENSOR.HOUSE_ID.eq(houseId))
                .and(SENSOR.ID.eq(sensorId))
                .execute();
    }
}
