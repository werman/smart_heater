package com.werman.heater.server.services.sensors.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class LastSensorsValue {

    @JsonProperty("id")
    private Integer sensorId;

    @JsonProperty("value")
    private Float lastValue;

    @JsonProperty("time")
    private Date lastTime;

    public void setSensorId(Integer sensorId) {
        this.sensorId = sensorId;
    }

    public void setLastValue(Float lastValue) {
        this.lastValue = lastValue;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }
}
