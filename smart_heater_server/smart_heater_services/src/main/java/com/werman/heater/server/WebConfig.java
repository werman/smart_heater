package com.werman.heater.server;

import com.werman.heater.server.services.security.AuthorizedMethodInterceptor;
import com.werman.heater.server.services.security.RequestUserAuthInterceptor;
import com.werman.heater.server.services.security.UserAuthMethodArgumentResolver;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public RequestUserAuthInterceptor requestUserAuthInterceptor() {
        return new RequestUserAuthInterceptor();
    }

    @Bean
    public AuthorizedMethodInterceptor authorizedMethodInterceptor() {
        return new AuthorizedMethodInterceptor();
    }

    @Bean
    public CORSInterceptor corsInterceptor() {
        return new CORSInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(corsInterceptor());
        registry.addInterceptor( requestUserAuthInterceptor() );
        registry.addInterceptor( authorizedMethodInterceptor() );
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        UserAuthMethodArgumentResolver userAuthMethodArgumentResolver = new UserAuthMethodArgumentResolver();
        argumentResolvers.add(0, userAuthMethodArgumentResolver);
    }

    @Bean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
    public DispatcherServlet dispatcherServlet()
    {
        return new DispatcherServlet();
    }

    @Bean
    public ServletRegistrationBean dispatcherRegistration()
    {
        ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet());
        registration.getInitParameters().put("dispatchOptionsRequest", "true");
        return registration;
    }
}
