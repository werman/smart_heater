package com.werman.heater.db.dao;

import com.werman.heater.db.data.tables.DBSensorTable;
import com.werman.heater.db.data.tables.daos.DBSensorDao;
import org.springframework.stereotype.Service;

import static org.jooq.impl.DSL.selectOne;
import static org.jooq.impl.DSL.using;

@Service
public class SensorDAO extends DBSensorDao {

    private final DBSensorTable SENSOR = DBSensorTable.SENSOR;

    public boolean isBelongToHouse(Integer sensorId, Integer houseId) {
        return using(configuration())
                       .fetchExists(selectOne()
                                            .from(SENSOR)
                                            .where(SENSOR.ID.eq(sensorId))
                                            .and(SENSOR.HOUSE_ID.eq(houseId)));
    }

}
