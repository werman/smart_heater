package com.werman.heater.server.services.security;

import com.werman.heater.server.services.security.responses.RegisterResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController("RegisterController")
@RequestMapping("/api/register")
public class RegisterController {

    @Autowired
    private UserService userService;

    @Autowired
    private LoginManager loginManager;

    @Autowired
    private JwtManager jwtManager;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public RegisterResponse register(@RequestParam String login,
                                     @RequestParam String password,
                                     HttpServletResponse response) {

        // TODO: Validation

        boolean success = userService.register(login, password);

        if (success) {
            SecurityUserInfo securityUserInfo = loginManager.loginAndGetSecurityInfo(login, password);

            if (securityUserInfo != null) {
                String token = jwtManager.createTokenForUser(securityUserInfo);
                return new RegisterResponse(token, login, "");
            }
            response.setStatus(HttpStatus.CREATED.value());
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        return new RegisterResponse("", login, "Error");
    }
}
