package com.werman.heater.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController("/api")
public class RegisterController {
    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

    @Autowired
    private UserService userService;

    @Autowired
    private LoginManager loginManager;

    @Autowired
    private JwtManager jwtManager;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void register(@RequestParam String email,
                           @RequestParam String password,
                           HttpServletResponse response) {

        // TODO: Validation

        boolean success = userService.register(email, password);

        if (success) {
            SecurityUserInfo securityUserInfo = loginManager.loginAndGetSecurityInfo(email, password);

            if (securityUserInfo != null) {
                response.setHeader(AUTH_HEADER_NAME, jwtManager.createTokenForUser(securityUserInfo));
            } else {

            }
        }
    }
}
