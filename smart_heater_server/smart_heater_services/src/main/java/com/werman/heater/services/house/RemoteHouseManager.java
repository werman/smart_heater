package com.werman.heater.services.house;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ning.http.client.*;
import com.werman.heater.services.house.commands.RemoteCommandResult;
import com.werman.heater.services.house.commands.RemoteHouseCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import java.io.IOException;

@Service
public class RemoteHouseManager {

    @Autowired
    private ObjectMapper json;

    @Autowired
    private AsyncHttpClient asyncHttpClient;

    private void sendCommandToLocalServer(String localServerUrl,
                                          String token,
                                          RemoteHouseCommand remoteHouseCommand,
                                          DeferredResult<RemoteCommandResult> resultHandler) throws JsonProcessingException {
        RequestBuilder requestBuilder = new RequestBuilder("POST");
        remoteHouseCommand.applyToRequest(requestBuilder, localServerUrl, json);
        requestBuilder.setHeader("TOKEN", token);
        Request request = requestBuilder.build();

        try {
            asyncHttpClient.prepareRequest(request).execute(new AsyncRemoteCommandHandler(resultHandler));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class AsyncRemoteCommandHandler extends AsyncCompletionHandler<Response> {

        private DeferredResult<RemoteCommandResult> resultHandler;

        public AsyncRemoteCommandHandler(DeferredResult<RemoteCommandResult> resultHandler) {
            this.resultHandler = resultHandler;
        }

        @Override
        public Response onCompleted(Response response) throws Exception {
            if (response.getStatusCode() == 400) {
                resultHandler.setResult(RemoteCommandResult.RESULT_SUCCESS);
            } else {
                resultHandler.setResult(RemoteCommandResult.RESULT_GENERIC_ERROR);
            }
            return response;
        }

        @Override
        public void onThrowable(Throwable t) {
            resultHandler.setResult(RemoteCommandResult.RESULT_REMOTE_HOST_UNREACHABLE);
        }
    }
}
