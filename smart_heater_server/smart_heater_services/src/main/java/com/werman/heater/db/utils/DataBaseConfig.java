package com.werman.heater.db.utils;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.jooq.ConnectionProvider;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.sql.SQLException;

@Configuration
@EnableTransactionManagement
@PropertySource(value = "classpath:database.properties")
public class DataBaseConfig {

    @Autowired
    private Environment env;

    @Bean
    public DataSource dataSource() throws PropertyVetoException, SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();

        dataSource.setDriverClass(env.getProperty("database.driver"));
        dataSource.setJdbcUrl(env.getProperty("database.url"));
        dataSource.setUser(env.getProperty("database.user"));
        dataSource.setPassword(env.getProperty("database.password"));
        dataSource.setMinPoolSize(env.getProperty("database.minPool", Integer.class));
        dataSource.setMaxPoolSize(env.getProperty("database.maxPool", Integer.class));
        dataSource.setIdleConnectionTestPeriod(env.getProperty("database.idleConnectionTestPeriod", Integer.class));
        dataSource.setLoginTimeout(env.getProperty("database.loginTimeout", Integer.class));
        dataSource.setBreakAfterAcquireFailure(env.getProperty("database.breakAfterAcquireFailure", Boolean.class));
        dataSource.setAcquireRetryDelay(env.getProperty("database.acquireRetryDelay", Integer.class));
        dataSource.setTestConnectionOnCheckin(env.getProperty("database.testConnectionOnCheckin", Boolean.class));

        return dataSource;
    }

    @Bean
    @Autowired
    public DataSourceTransactionManager transactionManager(DataSource dataSource) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource);
        return transactionManager;
    }

    @Bean
    @Autowired
    public DataSourceConnectionProvider connectionProvider(DataSource dataSource) {
        return new DataSourceConnectionProvider(new TransactionAwareDataSourceProxy(dataSource));
    }

    @Bean
    @Autowired
    public DefaultConfiguration dbConfiguration(ConnectionProvider connectionProvider) {
        DefaultConfiguration configuration = new DefaultConfiguration();
        configuration.setSQLDialect(SQLDialect.POSTGRES);
        configuration.setConnectionProvider(connectionProvider);
        configuration.setExecuteListenerProvider(new DefaultExecuteListenerProvider(new JOOQToSpringExceptionTransformer()));
        return configuration;
    }

    @Bean
    @Autowired
    public DefaultDSLContext dsl(DefaultConfiguration configuration) {
        return new DefaultDSLContext(configuration);
    }


}
