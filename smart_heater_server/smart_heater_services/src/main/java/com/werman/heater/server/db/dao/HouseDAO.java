package com.werman.heater.server.db.dao;

import com.werman.heater.server.db.data.tables.DBHouseTable;
import com.werman.heater.server.db.data.tables.daos.DBHouseDao;
import com.werman.heater.server.db.data.tables.records.DBHouseRecord;
import org.jooq.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.jooq.impl.DSL.using;

@Service
public class HouseDAO extends DBHouseDao {

    private final DBHouseTable HOUSE = DBHouseTable.HOUSE;

    @Autowired
    public HouseDAO(Configuration configuration) {
        super(configuration);
    }

    public DBHouseRecord fetchHouseWebInfoById(String id) {
        return using(configuration())
                       .select(HOUSE.SERVICE_ADDRESS, HOUSE.SERVICE_TOKEN)
                       .from(HOUSE)
                       .where(HOUSE.ID.eq(id))
                       .fetchOne()
                       .into(DBHouseRecord.class);
    }

    public void updateEndpoint(String houseId, String url, String token) {
        using(configuration())
                .update(HOUSE)
                .set(HOUSE.SERVICE_ADDRESS, url)
                .set(HOUSE.SERVICE_TOKEN, token)
                .where(HOUSE.ID.eq(houseId))
                .execute();
    }

    public int getOwnerId(String houseId) {
        return using(configuration())
                       .select(HOUSE.OWNER_ID)
                       .from(HOUSE)
                       .where(HOUSE.ID.eq(houseId))
                       .fetchOne()
                       .into(Integer.class);
    }

    public List<DBHouseRecord> fetchUserHousesMinimalInfo(Integer userId) {
        return using(configuration())
                .select(HOUSE.ID, HOUSE.NAME)
                .from(HOUSE)
                .where(HOUSE.OWNER_ID.eq(userId))
                .fetchInto(DBHouseRecord.class);
    }

    public void updateName(String houseId, String newName) {
        using(configuration())
                .update(HOUSE)
                .set(HOUSE.NAME, newName)
                .where(HOUSE.ID.eq(houseId))
                .execute();
    }
}
