/**
 * This class is generated by jOOQ
 */
package com.werman.heater.db.data.tables.pojos;


import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.0"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DBSensorHistoryEntry implements Serializable {

	private static final long serialVersionUID = 805313669;

	private Integer   sensorId;
	private Timestamp measureTime;
	private Float     value;

	public DBSensorHistoryEntry() {}

	public DBSensorHistoryEntry(DBSensorHistoryEntry value) {
		this.sensorId = value.sensorId;
		this.measureTime = value.measureTime;
		this.value = value.value;
	}

	public DBSensorHistoryEntry(
		Integer   sensorId,
		Timestamp measureTime,
		Float     value
	) {
		this.sensorId = sensorId;
		this.measureTime = measureTime;
		this.value = value;
	}

	public Integer getSensorId() {
		return this.sensorId;
	}

	public DBSensorHistoryEntry setSensorId(Integer sensorId) {
		this.sensorId = sensorId;
		return this;
	}

	public Timestamp getMeasureTime() {
		return this.measureTime;
	}

	public DBSensorHistoryEntry setMeasureTime(Timestamp measureTime) {
		this.measureTime = measureTime;
		return this;
	}

	public Float getValue() {
		return this.value;
	}

	public DBSensorHistoryEntry setValue(Float value) {
		this.value = value;
		return this;
	}
}
