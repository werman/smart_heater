package com.werman.heater.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController("/api")
public class LoginController {
    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

    @Autowired
    private LoginManager loginManager;

    @Autowired
    private JwtManager jwtManager;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void login(@RequestParam String login,
                      @RequestParam String password,
                      HttpServletResponse response) {
        SecurityUserInfo securityUserInfo = loginManager.loginAndGetSecurityInfo(login, password);

        if (securityUserInfo != null) {
            response.setHeader(AUTH_HEADER_NAME, jwtManager.createTokenForUser(securityUserInfo));
            // TODO redirect to home
        }
    }
}
