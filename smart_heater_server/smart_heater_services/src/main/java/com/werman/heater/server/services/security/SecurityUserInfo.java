package com.werman.heater.server.services.security;

import java.util.List;

public class SecurityUserInfo {

    private long expireDate;

    private int id;

    private List<String> ownedHouses;

    public long getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(long expireDate) {
        this.expireDate = expireDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getOwnedHouses() {
        return ownedHouses;
    }

    public void setOwnedHouses(List<String> ownedHouses) {
        this.ownedHouses = ownedHouses;
    }

    public boolean ownHouse(String houseId) {
        return ownedHouses.contains(houseId);
    }
}
