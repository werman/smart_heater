package com.werman.heater.services.sensors.responses;

import java.util.Date;

public class SensorStatisticFrame {

    private Date date;

    private Float value;

    private Integer measuresCount;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Integer getMeasuresCount() {
        return measuresCount;
    }

    public void setMeasuresCount(Integer measuresCount) {
        this.measuresCount = measuresCount;
    }
}
