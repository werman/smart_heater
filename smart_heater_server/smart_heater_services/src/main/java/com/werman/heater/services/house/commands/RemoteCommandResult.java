package com.werman.heater.services.house.commands;

import com.fasterxml.jackson.annotation.JsonValue;

public enum RemoteCommandResult {
    RESULT_SUCCESS("Success"),
    RESULT_REMOTE_HOST_UNREACHABLE("Remote host is unreachable"),
    RESULT_GENERIC_ERROR("Error");

    private String name;

    RemoteCommandResult(String name) {
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }
}
