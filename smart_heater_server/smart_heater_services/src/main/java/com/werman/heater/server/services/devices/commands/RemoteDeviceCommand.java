package com.werman.heater.server.services.devices.commands;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ning.http.client.FluentStringsMap;
import com.ning.http.client.RequestBuilder;

import java.util.HashMap;
import java.util.Map;

public abstract class RemoteDeviceCommand {

    @JsonIgnore
    private String methodName;

    private Map<String, String> params = new HashMap<>();

    protected RemoteDeviceCommand() {

    }

    protected void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    protected void addParam(String name, String value) {
        params.put(name, value);
    }

    public void applyToRequest(RequestBuilder requestBuilder, String baseUrl, ObjectMapper json) throws JsonProcessingException {
        //String payload = json.writeValueAsString(this);
        requestBuilder.setUrl(baseUrl + "control/" + methodName);
        requestBuilder.addHeader("Content-Type", "application/json");

        FluentStringsMap parameters = new FluentStringsMap();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            parameters.add(entry.getKey(), entry.getValue());
            //requestBuilder.setQueryParameters().addParameter(entry.getKey(), entry.getValue());
        }

        requestBuilder.setQueryParameters(parameters);

        //requestBuilder.setBody(payload);
    }
}
