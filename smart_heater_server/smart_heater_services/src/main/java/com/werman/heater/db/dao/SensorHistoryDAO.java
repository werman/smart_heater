package com.werman.heater.db.dao;

import com.werman.heater.db.data.tables.DBSensorHistoryEntryTable;
import com.werman.heater.db.data.tables.daos.DBSensorHistoryEntryDao;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.Table;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.jooq.impl.DSL.*;

@Service
public class SensorHistoryDAO extends DBSensorHistoryEntryDao {

    private final DBSensorHistoryEntryTable SENSOR_HISTORY = DBSensorHistoryEntryTable.SENSOR_HISTORY_ENTRY;

    public Map<Integer, Float> getLastSensorsValues(List<Integer> ids) {
        DBSensorHistoryEntryTable tableA = SENSOR_HISTORY.as("t1");
        DBSensorHistoryEntryTable tableB = SENSOR_HISTORY.as("t2");

        Table<Record2<Integer, Timestamp>> joinQuery = using(configuration())
                                                               .select(tableB.SENSOR_ID, max(tableB.MEASURE_TIME)
                                                                                                 .as("maxTime"))
                                                               .from(tableB)
                                                               .where(tableB.SENSOR_ID.in(ids))
                                                               .groupBy(tableB.SENSOR_ID)
                                                               .asTable();

        Result<Record2<Integer, Float>> queryResults = using(configuration())
                                               .fetch(select(tableA.SENSOR_ID, tableA.VALUE)
                                                                  .from(tableA)
                                                                  .join(joinQuery)
                                                                  .on(tableA.SENSOR_ID.eq(tableB.SENSOR_ID)
                                                                              .and(tableA.MEASURE_TIME.eq((Field<Timestamp>) joinQuery.field("maxTime"))))
                                               );

        Map<Integer, Float> results = new HashMap<>();

        queryResults.forEach(record -> results.put((Integer)record.getValue(0), (Float)record.getValue(1)));

        return results;
    }
}
