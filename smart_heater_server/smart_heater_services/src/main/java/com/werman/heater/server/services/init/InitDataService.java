package com.werman.heater.server.services.init;

import com.werman.heater.server.db.dao.DeviceDAO;
import com.werman.heater.server.db.dao.HouseDAO;
import com.werman.heater.server.db.dao.SensorDAO;
import com.werman.heater.server.db.data.tables.records.DBDeviceRecord;
import com.werman.heater.server.db.data.tables.records.DBHouseRecord;
import com.werman.heater.server.db.data.tables.records.DBSensorRecord;
import com.werman.heater.server.services.init.data.DeviceInfo;
import com.werman.heater.server.services.init.data.HouseInfo;
import com.werman.heater.server.services.init.data.SensorInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class InitDataService {

    @Autowired
    private HouseDAO houseDAO;

    @Autowired
    private SensorDAO sensorDAO;

    @Autowired
    private DeviceDAO deviceDAO;

    public List<HouseInfo> getUserHousesDetails(Integer userId) {
        List<HouseInfo> houseInfos = new ArrayList<>();

        List<DBHouseRecord> houseRecords = houseDAO.fetchUserHousesMinimalInfo(userId);

        for (DBHouseRecord record : houseRecords) {
            HouseInfo houseInfo = new HouseInfo();
            houseInfo.setId(record.getId());
            houseInfo.setName(record.getName());

            List<DBSensorRecord> sensorRecords = sensorDAO.getSensorsMinimalInfoByHouseId(record.getId());

            List<SensorInfo> sensorInfos = new ArrayList<>();

            for (DBSensorRecord sensorRecord : sensorRecords) {
                SensorInfo sensorInfo = new SensorInfo();
                sensorInfo.setId(sensorRecord.getId());
                sensorInfo.setName(sensorRecord.getName());
                sensorInfo.setLastMeasuredValue(sensorRecord.getLastValue());

                if (sensorRecord.getLastMeasureTime() != null) {
                    sensorInfo.setLastMeasureTime(new Date(sensorRecord.getLastMeasureTime().getTime()));
                }

                sensorInfos.add(sensorInfo);
            }

            List<DBDeviceRecord> deviceRecords = deviceDAO.getDevicesMinimalInfoByHouseId(record.getId());

            List<DeviceInfo> deviceInfos = new ArrayList<>();

            for (DBDeviceRecord deviceRecord : deviceRecords) {
                DeviceInfo deviceInfo = new DeviceInfo();
                deviceInfo.setId(deviceRecord.getId());
                deviceInfo.setName(deviceRecord.getName());
                deviceInfo.setTarget(deviceRecord.getTargetValue());

                if (deviceRecord.getTurnedOn()) {
                    deviceInfo.setState("on");
                } else {
                    deviceInfo.setState("off");
                }

                deviceInfos.add(deviceInfo);
            }

            houseInfo.setSensors(sensorInfos);
            houseInfo.setDevices(deviceInfos);

            houseInfos.add(houseInfo);
        }

        return houseInfos;
    }
}
