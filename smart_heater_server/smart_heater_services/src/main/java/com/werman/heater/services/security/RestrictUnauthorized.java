package com.werman.heater.services.security;

import com.werman.heater.services.security.restrictions.Restriction;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RestrictUnauthorized {
    Class<? extends Restriction>[] restrictions() default {};
}
