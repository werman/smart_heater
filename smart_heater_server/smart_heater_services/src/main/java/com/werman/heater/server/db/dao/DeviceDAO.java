package com.werman.heater.server.db.dao;

import com.werman.heater.server.db.data.tables.DBDeviceTable;
import com.werman.heater.server.db.data.tables.daos.DBDeviceDao;
import com.werman.heater.server.db.data.tables.records.DBDeviceRecord;
import org.jooq.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.jooq.impl.DSL.using;

@Service
public class DeviceDAO extends DBDeviceDao {

    private final DBDeviceTable DEVICE = DBDeviceTable.DEVICE;

    @Autowired
    public DeviceDAO(Configuration configuration) {
        super(configuration);
    }

    public String insertAndGetId(DBDeviceRecord record) {
        DBDeviceRecord result = using(configuration())
                                        .insertInto(DEVICE)
                                        .set(record)
                                        .returning(DEVICE.ID)
                                        .fetchOne();
        return result.getId();
    }

    public List<DBDeviceRecord> getDevicesMinimalInfoByHouseId(String houseId) {
        return using(configuration())
                       .select(DEVICE.ID, DEVICE.NAME, DEVICE.TARGET_VALUE, DEVICE.TURNED_ON)
                       .from(DEVICE)
                       .where(DEVICE.HOUSE_ID.eq(houseId))
                       .fetchInto(DBDeviceRecord.class);
    }

    public void updateDeviceState(String houseId, String deviceId, boolean turnedOn) {
        using(configuration())
                .update(DEVICE)
                .set(DEVICE.TURNED_ON, turnedOn)
                .where(DEVICE.ID.eq(deviceId))
                .and(DEVICE.HOUSE_ID.eq(houseId))
                .execute();
    }

    public void updateDeviceName(String houseId, String deviceId, String newName) {
        using(configuration())
                .update(DEVICE)
                .set(DEVICE.NAME, newName)
                .where(DEVICE.ID.eq(deviceId))
                .and(DEVICE.HOUSE_ID.eq(houseId))
                .execute();
    }

    public void updateTargetValue(String houseId, String deviceId, Float targetValue) {
        using(configuration())
                .update(DEVICE)
                .set(DEVICE.TARGET_VALUE, targetValue)
                .where(DEVICE.ID.eq(deviceId))
                .and(DEVICE.HOUSE_ID.eq(houseId))
                .execute();
    }
}
