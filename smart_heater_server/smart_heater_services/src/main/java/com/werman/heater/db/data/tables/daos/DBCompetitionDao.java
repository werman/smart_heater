/**
 * This class is generated by jOOQ
 */
package com.werman.heater.db.data.tables.daos;


import com.werman.heater.db.data.tables.DBCompetitionTable;
import com.werman.heater.db.data.tables.pojos.DBCompetition;
import com.werman.heater.db.data.tables.records.DBCompetitionRecord;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.0"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DBCompetitionDao extends DAOImpl<DBCompetitionRecord, DBCompetition, Integer> {

	/**
	 * Create a new DBCompetitionDao without any configuration
	 */
	public DBCompetitionDao() {
		super(DBCompetitionTable.COMPETITION, DBCompetition.class);
	}

	/**
	 * Create a new DBCompetitionDao with an attached configuration
	 */
	public DBCompetitionDao(Configuration configuration) {
		super(DBCompetitionTable.COMPETITION, DBCompetition.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Integer getId(DBCompetition object) {
		return object.getId();
	}

	/**
	 * Fetch records that have <code>id IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBId(Integer... values) {
		return fetch(DBCompetitionTable.COMPETITION.ID, values);
	}

	/**
	 * Fetch a unique record that has <code>id = value</code>
	 */
	public DBCompetition fetchOneByDBId(Integer value) {
		return fetchOne(DBCompetitionTable.COMPETITION.ID, value);
	}

	/**
	 * Fetch records that have <code>description IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBDescription(String... values) {
		return fetch(DBCompetitionTable.COMPETITION.DESCRIPTION, values);
	}

	/**
	 * Fetch records that have <code>end_date IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBEndDate(Timestamp... values) {
		return fetch(DBCompetitionTable.COMPETITION.END_DATE, values);
	}

	/**
	 * Fetch records that have <code>image IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBImage(String... values) {
		return fetch(DBCompetitionTable.COMPETITION.IMAGE, values);
	}

	/**
	 * Fetch records that have <code>name IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBName(String... values) {
		return fetch(DBCompetitionTable.COMPETITION.NAME, values);
	}

	/**
	 * Fetch records that have <code>start_date IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBStartDate(Timestamp... values) {
		return fetch(DBCompetitionTable.COMPETITION.START_DATE, values);
	}

	/**
	 * Fetch records that have <code>type IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBType(String... values) {
		return fetch(DBCompetitionTable.COMPETITION.TYPE, values);
	}

	/**
	 * Fetch records that have <code>owner_id IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBOwnerId(Integer... values) {
		return fetch(DBCompetitionTable.COMPETITION.OWNER_ID, values);
	}

	/**
	 * Fetch records that have <code>visible IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBVisible(Boolean... values) {
		return fetch(DBCompetitionTable.COMPETITION.VISIBLE, values);
	}

	/**
	 * Fetch records that have <code>participants_count IN (values)</code>
	 */
	public List<DBCompetition> fetchByDBParticipantsCount(Integer... values) {
		return fetch(DBCompetitionTable.COMPETITION.PARTICIPANTS_COUNT, values);
	}
}
