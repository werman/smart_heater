package com.werman.heater.db.dao;

import com.werman.heater.db.data.tables.DBHouseTable;
import com.werman.heater.db.data.tables.daos.DBHouseDao;
import org.springframework.stereotype.Service;

@Service
public class HouseDAO extends DBHouseDao {

    private final DBHouseTable HOUSE = DBHouseTable.HOUSE;
}
