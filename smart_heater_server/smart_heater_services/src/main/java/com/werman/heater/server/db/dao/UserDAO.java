package com.werman.heater.server.db.dao;

import com.werman.heater.server.db.data.tables.DBUserDetailsTable;
import com.werman.heater.server.db.data.tables.daos.DBUserDetailsDao;
import com.werman.heater.server.db.data.tables.records.DBUserDetailsRecord;
import org.jooq.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.jooq.impl.DSL.selectOne;
import static org.jooq.impl.DSL.using;

@Service
public class UserDAO extends DBUserDetailsDao {

    private final DBUserDetailsTable USER = DBUserDetailsTable.USER_DETAILS;

    @Autowired
    public UserDAO(Configuration configuration) {
        super(configuration);
    }

    public boolean isUserWithEmailExists(String email) {
        return using(configuration())
                       .fetchExists(
                                           selectOne()
                                                   .from(USER)
                                                   .where(USER.EMAIL.eq(email)));
    }

    public DBUserDetailsRecord findByEmail(String email) {
        return using(configuration())
                .fetchOne(USER, USER.EMAIL.eq(email))
                       .into(DBUserDetailsRecord.class);
    }
}
