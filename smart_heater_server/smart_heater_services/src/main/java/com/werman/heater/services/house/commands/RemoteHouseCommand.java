package com.werman.heater.services.house.commands;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ning.http.client.RequestBuilder;

public abstract class RemoteHouseCommand {

    @JsonIgnore
    private String methodName;

    protected RemoteHouseCommand() {

    }

    protected void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public void applyToRequest(RequestBuilder requestBuilder, String baseUrl, ObjectMapper json) throws JsonProcessingException {
        String payload = json.writeValueAsString(this);
        requestBuilder.setUrl(baseUrl + methodName);
        requestBuilder.addHeader("Content-Type", "application/json");
        requestBuilder.setBody(payload);
    }
}
