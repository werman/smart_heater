package com.werman.heater.services.test;

import com.werman.heater.services.security.RestrictUnauthorized;
import com.werman.heater.services.security.SecurityUserInfo;
import com.werman.heater.services.security.UserAuth;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestService {

    @RequestMapping("/test")
    public String getTestString() {
        return "test";
    }

    @RestrictUnauthorized
    @RequestMapping("/test_authorized")
    public String getAuthorizedTestString(@UserAuth SecurityUserInfo securityUserInfo) {
        return "test " + Long.toString(securityUserInfo.getExpireDate());
    }
}
