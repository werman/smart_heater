package com.werman.heater.server.services.sensors;

import com.werman.heater.server.db.dao.SensorDAO;
import com.werman.heater.server.db.dao.SensorHistoryDAO;
import com.werman.heater.server.db.data.enums.DBSensorType;
import com.werman.heater.server.db.data.tables.pojos.DBSensor;
import com.werman.heater.server.db.data.tables.pojos.DBSensorHistoryEntry;
import com.werman.heater.server.db.data.tables.records.DBSensorHistoryEntryRecord;
import com.werman.heater.server.db.data.tables.records.DBSensorRecord;
import com.werman.heater.server.services.security.SecurityUserInfo;
import com.werman.heater.server.services.sensors.responses.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

@Service
public class SensorsService {

    @Autowired
    private SensorDAO sensorDAO;

    @Autowired
    private SensorHistoryDAO sensorHistoryDAO;

    public RegisterSensorResult registerSensor(String sensorUUID, String houseId, DBSensorType type) {
        if (sensorDAO.exists(sensorUUID, houseId)) {
            Integer sensorId = sensorDAO.getSensorId(sensorUUID, houseId);
            return new RegisterSensorResult(sensorId, "Exists");
        }

        DBSensorRecord record = new DBSensorRecord();
        record.setIdentifier(sensorUUID);
        record.setHouseId(houseId);
        record.setName("No name");
        record.setType(type);

        Integer id = sensorDAO.insertAndGetId(record);
        return new RegisterSensorResult(id, "Success");
    }

    public boolean addSensorValue(SecurityUserInfo securityUserInfo, Integer sensorId, Float value) {
        DBSensor sensor = sensorDAO.findById(sensorId);

        if (sensor != null) {
            if (securityUserInfo.ownHouse(sensor.getHouseId())) {
                Date now = Calendar.getInstance().getTime();
                DBSensorHistoryEntry sensorHistoryEntry = new DBSensorHistoryEntry();
                sensorHistoryEntry
                        .setSensorId(sensorId)
                        .setValue(value)
                        .setMeasureTime(new Timestamp(now.getTime()));

                sensorHistoryDAO.insert(sensorHistoryEntry);

                sensorDAO.updateLastMeasure(sensor.getHouseId(), sensorId, value, now);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public MultiplySensorsStatisticInPeriod getSensorsStatistic(SecurityUserInfo securityUserInfo,
                                                                List<Integer> sensorIds,
                                                                Date startDate,
                                                                Date endDate,
                                                                Integer granularityInSeconds) {

        MultiplySensorsStatisticInPeriod statistic = new MultiplySensorsStatisticInPeriod();

        Map<Integer, SensorStatisticInPeriod> statistics = new HashMap<>();

        for (Integer sensorId : sensorIds) {
            SensorStatisticInPeriod sensorStatisticInPeriod = new SensorStatisticInPeriod();
            List<DBSensorHistoryEntryRecord> history = sensorHistoryDAO.getHistory(sensorId, startDate, endDate);
            sensorStatisticInPeriod.setStartDate(startDate);
            sensorStatisticInPeriod.setEndDate(endDate);
            sensorStatisticInPeriod.setMeasuresCount(history.size());
            sensorStatisticInPeriod.setGranularity(granularityInSeconds);

            int framesCount = (int) (endDate.getTime() - startDate.getTime()) / (granularityInSeconds * 1000) + 1;

            List<SensorStatisticFrame> frames = new ArrayList<>(framesCount);

            for (int i = 0; i < framesCount; i++) {
                frames.add(new SensorStatisticFrame());
            }

            for (DBSensorHistoryEntryRecord record : history) {
                int offset = (int) (record.getMeasureTime().getTime() - startDate.getTime()) / (granularityInSeconds * 1000);
                SensorStatisticFrame frame = frames.get(offset);
                frame.setMeasuresCount(frame.getMeasuresCount() + 1);
                frame.setValue(frame.getValue() + record.getValue());
            }

            for (int i = 0; i < framesCount; i++) {
                SensorStatisticFrame frame = frames.get(i);
                frame.setValue(frame.getValue() / frame.getMeasuresCount());
                frame.setDate(new Date(startDate.getTime() + i * (granularityInSeconds * 1000) + granularityInSeconds * 500));
            }

            sensorStatisticInPeriod.setData(frames);

            statistics.put(sensorId, sensorStatisticInPeriod);
        }
        statistic.setStatistic(statistics);
        return statistic;
    }

    public List<LastSensorsValue> getLastSensorsValues(SecurityUserInfo securityUserInfo, List<Integer> sensorIds) {
        List<DBSensorRecord> lastValues = sensorDAO.getLastSensorsValues(sensorIds);
        List<LastSensorsValue> lastSensorsValue = new ArrayList<>();

        for (DBSensorRecord record : lastValues) {
            LastSensorsValue sensorsValue = new LastSensorsValue();
            sensorsValue.setSensorId(record.getId());
            sensorsValue.setLastValue(record.getLastValue());
            sensorsValue.setLastTime(new Date(record.getLastMeasureTime().getTime()));
            lastSensorsValue.add(sensorsValue);
        }

        return lastSensorsValue;
    }

    public void changeSensorName(String houseId, Integer sensorId, String newName) {
        sensorDAO.updateSensorName(houseId, sensorId, newName);
    }

    private boolean between(DBSensorHistoryEntryRecord record, Date startDate, Date endDate) {
        return record.getMeasureTime().getTime() > startDate.getTime()
                       && record.getMeasureTime().getTime() < endDate.getTime();
    }
}
