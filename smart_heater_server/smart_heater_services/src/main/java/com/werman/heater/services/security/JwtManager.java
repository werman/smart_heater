package com.werman.heater.services.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class JwtManager {

    @Autowired
    private ObjectMapper json;

    @Value("${security.rsa.private}")
    private String rsaKey;

    private Signer signer;

    private SignatureVerifier verifier;

    @PostConstruct
    public void init() {
        signer = new RsaSigner(rsaKey);
        verifier = new RsaVerifier(rsaKey);
    }

    public String createTokenForUser(SecurityUserInfo user) {
        try {
            Jwt jwt = JwtHelper.encode(json.writeValueAsString(user), signer);
            return jwt.getEncoded();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public SecurityUserInfo getUserFromToken(String token) {
        if (token == null || token.isEmpty()) {
            return null;
        }

        try {
            Jwt jwt = JwtHelper.decodeAndVerify(token, verifier);
            String jsonString = jwt.getClaims();
            SecurityUserInfo user = json.readValue(jsonString, SecurityUserInfo.class);
            return user;
        } catch (InvalidSignatureException ex) {
            ex.printStackTrace();
            return null;
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
