package com.werman.heater.server.services.devices.commands;

public class ChangeTargetCommand extends RemoteDeviceCommand {

    private ChangeTargetCommand() {
        super();
    }

    public static RemoteDeviceCommand create(String deviceId, Float targetTemperature) {
        ChangeTargetCommand command = new ChangeTargetCommand();
        command.setMethodName("change_target");
        command.addParam("id", deviceId);
        command.addParam("target", targetTemperature.toString());
        return command;
    }
}
