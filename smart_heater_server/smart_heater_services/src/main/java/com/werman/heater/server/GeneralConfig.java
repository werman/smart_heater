package com.werman.heater.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GeneralConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public AsyncHttpClient asyncHttpClient() {
        AsyncHttpClientConfig cf = new AsyncHttpClientConfig.Builder()
                .setAllowPoolingConnection(true)
                .setAllowSslConnectionPool(true)
                .setMaxRequestRetry(2)
                .setFollowRedirects(false)
                .build();
        return new AsyncHttpClient(cf);
    }
}
