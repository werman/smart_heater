package com.werman.heater.server.services.devices;

import com.werman.heater.server.db.dao.DeviceDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DevicesService {

    @Autowired
    private DeviceDAO deviceDAO;

    public void setDeviceState(String houseId, String deviceId, String state) {
        boolean enabled = false;
        if (state.equals("on")) {
            enabled = true;
        }

        deviceDAO.updateDeviceState(houseId, deviceId, enabled);
    }

    public void changeDeviceName(String houseId, String deviceId, String newName) {
        deviceDAO.updateDeviceName(houseId, deviceId, newName);
    }

    public void changeTargetValue(String houseId, String deviceId, Float newTargetValue) {
        deviceDAO.updateTargetValue(houseId, deviceId, newTargetValue);
    }
}
