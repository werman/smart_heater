package com.werman.heater.server.services.security;

import com.werman.heater.server.services.security.responses.LoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController("LoginController")
@RequestMapping("/api/login")
public class LoginController {

    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

    @Autowired
    private JwtManager jwtManager;

    @Autowired
    private LoginManager loginManager;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public LoginResponse login(@RequestParam String login,
                               @RequestParam String password,
                               HttpServletResponse response) {
        SecurityUserInfo securityUserInfo = loginManager.loginAndGetSecurityInfo(login, password);

        if (securityUserInfo != null) {
            String token = jwtManager.createTokenForUser(securityUserInfo);
            response.setStatus(HttpStatus.ACCEPTED.value());
            return new LoginResponse(token, login, "");
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return new LoginResponse("", "", "Unknown user");
        }
    }
}
