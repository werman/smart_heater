package com.werman.heater.server.services.sensors.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterSensorResult {

    @JsonProperty("sensor_id")
    private Integer sensorId;
    @JsonProperty("result")
    private String result;

    public RegisterSensorResult(Integer sensorId, String result) {
        this.sensorId = sensorId;
        this.result = result;
    }

    public Integer getSensorId() {
        return sensorId;
    }

    public void setSensorId(Integer sensorId) {
        this.sensorId = sensorId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
