package com.werman.heater.server.rest;

import com.werman.heater.server.services.security.RestrictUnauthorized;
import com.werman.heater.server.services.security.SecurityUserInfo;
import com.werman.heater.server.services.security.UserAuth;
import com.werman.heater.server.services.security.restrictions.HouseOwnerRestriction;
import com.werman.heater.server.services.sensors.SensorsService;
import com.werman.heater.server.services.sensors.responses.LastSensorsValue;
import com.werman.heater.server.services.sensors.responses.MultiplySensorsStatisticInPeriod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController("SensorsController")
@RequestMapping("/api/house/sensors")
public class SensorsController {

    @Autowired
    private SensorsService sensorsService;

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public void getHouseSensors(@RequestParam Integer houseId) {

    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/name", method = RequestMethod.POST)
    public void changeSensorName(@RequestParam String houseId,
                                 @RequestParam Integer sensorId,
                                 @RequestParam String newName) {
        sensorsService.changeSensorName(houseId, sensorId, newName);
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/last_value", method = RequestMethod.GET)
    public List<LastSensorsValue> getLastValues(@RequestParam List<Integer> sensorsId,
                                           @UserAuth SecurityUserInfo securityUserInfo) {
        return sensorsService.getLastSensorsValues(securityUserInfo, sensorsId);
    }

    @RestrictUnauthorized()
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public
    @ResponseBody
    MultiplySensorsStatisticInPeriod getTemperatureInfo(@RequestParam List<Integer> sensorUUIDs,
                                                        @RequestParam @DateTimeFormat(pattern="dd-MM-yyyy HH:mm") Date startDate,
                                                        @RequestParam @DateTimeFormat(pattern="dd-MM-yyyy HH:mm") Date endDate,
                                                        @RequestParam Integer granularityInSeconds,
                                                        @UserAuth SecurityUserInfo securityUserInfo) {
        //TODO: validate date interval

        return sensorsService.getSensorsStatistic(securityUserInfo, sensorUUIDs, startDate, endDate, granularityInSeconds);
    }
}
