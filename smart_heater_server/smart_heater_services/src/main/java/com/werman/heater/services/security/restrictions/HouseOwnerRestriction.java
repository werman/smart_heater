package com.werman.heater.services.security.restrictions;

import com.werman.heater.services.security.SecurityUserInfo;

import javax.servlet.http.HttpServletRequest;

public class HouseOwnerRestriction implements Restriction {

    @Override
    public boolean hasAccess(HttpServletRequest httpRequest, SecurityUserInfo securityUserInfo) {
        return true;
    }
}
