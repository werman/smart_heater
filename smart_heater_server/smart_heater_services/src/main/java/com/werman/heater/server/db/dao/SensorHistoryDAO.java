package com.werman.heater.server.db.dao;

import com.werman.heater.server.db.data.tables.DBSensorHistoryEntryTable;
import com.werman.heater.server.db.data.tables.daos.DBSensorHistoryEntryDao;
import com.werman.heater.server.db.data.tables.records.DBSensorHistoryEntryRecord;
import org.jooq.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.jooq.impl.DSL.using;

@Service
public class SensorHistoryDAO extends DBSensorHistoryEntryDao {

    private final DBSensorHistoryEntryTable SENSOR_HISTORY = DBSensorHistoryEntryTable.SENSOR_HISTORY_ENTRY;

    @Autowired
    public SensorHistoryDAO(Configuration configuration) {
        super(configuration);
    }

    public List<DBSensorHistoryEntryRecord> getHistory(Integer sensorId, Date startDate, Date endDate) {
        Timestamp start = new Timestamp(startDate.getTime());
        Timestamp end = new Timestamp(endDate.getTime());
        return using(configuration())
                       .fetch(SENSOR_HISTORY,
                                     SENSOR_HISTORY.SENSOR_ID.eq(sensorId).and(
                                                                                      SENSOR_HISTORY.MEASURE_TIME.between(start, end)))
                       .into(DBSensorHistoryEntryRecord.class);
    }
}
