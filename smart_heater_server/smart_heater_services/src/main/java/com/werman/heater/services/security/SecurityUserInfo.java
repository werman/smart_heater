package com.werman.heater.services.security;

import java.util.List;

public class SecurityUserInfo {

    private long expireDate;

    private List<Integer> ownedHouses;

    public long getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(long expireDate) {
        this.expireDate = expireDate;
    }

    public List<Integer> getOwnedHouses() {
        return ownedHouses;
    }

    public void setOwnedHouses(List<Integer> ownedHouses) {
        this.ownedHouses = ownedHouses;
    }

    public boolean ownHouse(Integer houseId) {
        return ownedHouses.contains(houseId);
    }
}
