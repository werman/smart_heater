package com.werman.heater.server.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;

public class RequestUserAuthInterceptor extends HandlerInterceptorAdapter {
    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

    @Autowired
    private JwtManager jwtManager;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        final String token = request.getHeader(AUTH_HEADER_NAME);

        SecurityUserInfo user = jwtManager.getUserFromToken(token);

        if (user != null) {
            long now = Calendar.getInstance().getTimeInMillis();
            if (now > user.getExpireDate()) {
                // Expired, redirect to unauthorized
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return false;
            } else {
                request.setAttribute("securityUser", user);
                return true;
            }

        }

        return true;
    }
}
