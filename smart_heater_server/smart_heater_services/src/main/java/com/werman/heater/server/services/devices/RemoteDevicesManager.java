package com.werman.heater.server.services.devices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ning.http.client.*;
import com.werman.heater.server.db.dao.DeviceDAO;
import com.werman.heater.server.db.dao.HouseDAO;
import com.werman.heater.server.db.data.enums.DBDeviceType;
import com.werman.heater.server.db.data.tables.pojos.DBDevice;
import com.werman.heater.server.db.data.tables.records.DBHouseRecord;
import com.werman.heater.server.services.devices.commands.RemoteCommandResult;
import com.werman.heater.server.services.devices.commands.RemoteDeviceCommand;
import com.werman.heater.server.services.devices.responses.RegisterDeviceResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import java.io.IOException;

@Service
public class RemoteDevicesManager {

    @Autowired
    private HouseDAO houseDAO;

    @Autowired
    private DeviceDAO deviceDAO;

    @Autowired
    private ObjectMapper json;

    @Autowired
    private AsyncHttpClient asyncHttpClient;

    public RegisterDeviceResult registerDevice(String houseId, String deviceId, DBDeviceType type, String description) {
        DBDevice device = new DBDevice();
        device.setId(deviceId);
        device.setHouseId(houseId);
        device.setType(type);
        device.setName(description);
        device.setTurnedOn(false);
        device.setTargetValue(0f);

        deviceDAO.insert(device);
        return new RegisterDeviceResult(deviceId, "Success");
    }

    public void sendCommandToUsersHouse(String houseId,
                                        RemoteDeviceCommand remoteDeviceCommand,
                                        DeferredResult<RemoteCommandResult> resultHandler) {
        DBHouseRecord house = houseDAO.fetchHouseWebInfoById(houseId);

        if (house.getServiceAddress() == null) {
            resultHandler.setResult(RemoteCommandResult.RESULT_ENDPOINT_NO_REGISTERED);
            return;
        }

        sendCommandToLocalServer(house.getServiceAddress(), house.getServiceToken(), remoteDeviceCommand, resultHandler);

        //resultHandler.setResult(RemoteCommandResult.RESULT_SUCCESS);
    }

    private void sendCommandToLocalServer(String localServerUrl,
                                          String token,
                                          RemoteDeviceCommand remoteDeviceCommand,
                                          DeferredResult<RemoteCommandResult> resultHandler) {
        RequestBuilder requestBuilder = new RequestBuilder("POST");

        try {
            remoteDeviceCommand.applyToRequest(requestBuilder, localServerUrl, json);
        } catch (JsonProcessingException e) {
            resultHandler.setResult(RemoteCommandResult.RESULT_INTERNAL_ERROR);
            e.printStackTrace();
        }

        requestBuilder.setHeader("TOKEN", token);
        Request request = requestBuilder.build();

        try {
            asyncHttpClient.prepareRequest(request).execute(new AsyncRemoteCommandHandler(resultHandler));
        } catch (IOException e) {
            resultHandler.setResult(RemoteCommandResult.RESULT_INTERNAL_ERROR);
            e.printStackTrace();
        }
    }

    private static class AsyncRemoteCommandHandler extends AsyncCompletionHandler<Response> {

        private DeferredResult<RemoteCommandResult> resultHandler;

        public AsyncRemoteCommandHandler(DeferredResult<RemoteCommandResult> resultHandler) {
            this.resultHandler = resultHandler;
        }

        @Override
        public Response onCompleted(Response response) throws Exception {
            if (response.getStatusCode() == 200 || response.getStatusCode() == 204) {
                resultHandler.setResult(RemoteCommandResult.RESULT_SUCCESS);
            } else {
                resultHandler.setResult(RemoteCommandResult.RESULT_GENERIC_ERROR);
            }
            return response;
        }

        @Override
        public void onThrowable(Throwable t) {
            resultHandler.setResult(RemoteCommandResult.RESULT_REMOTE_HOST_UNREACHABLE);
        }
    }
}
