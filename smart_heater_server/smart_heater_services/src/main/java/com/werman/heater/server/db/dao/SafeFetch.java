package com.werman.heater.server.db.dao;

import org.jooq.Record;

public class SafeFetch {

    static public <T> T fetchInto(Record record, Class<T> clazz) {
        if (record == null) {
            return null;
        }
        return record.into(clazz);
    }
}
