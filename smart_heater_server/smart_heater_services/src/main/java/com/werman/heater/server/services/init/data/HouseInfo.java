package com.werman.heater.server.services.init.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
* Created by wermaan on 06.06.2015.
*/
public class HouseInfo {
    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("sensors")
    private List<SensorInfo> sensors;

    @JsonProperty("devices")
    private List<DeviceInfo> devices;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SensorInfo> getSensors() {
        return sensors;
    }

    public void setSensors(List<SensorInfo> sensors) {
        this.sensors = sensors;
    }

    public List<DeviceInfo> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceInfo> devices) {
        this.devices = devices;
    }
}
