package com.werman.heater.server.services.security;

import com.werman.heater.server.db.dao.HouseDAO;
import com.werman.heater.server.db.data.tables.pojos.DBHouse;
import com.werman.heater.server.db.data.tables.records.DBUserDetailsRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LoginManager {

    @Value("${security.token.expire}")
    private Long EXPIRE_DELTA;

    @Autowired
    private UserService userService;

    @Autowired
    private CryptService cryptService;

    @Autowired
    private HouseDAO houseDAO;

    public SecurityUserInfo loginAndGetSecurityInfo(String login, String password) {
        DBUserDetailsRecord userDetails = userService.getUserDetails(login);

        if (userDetails != null) {

            String passwordHash = cryptService.getPasswordHash(password);

            if (userDetails.getPasswordHash().equals(passwordHash)) {
                long now = Calendar.getInstance().getTimeInMillis();
                SecurityUserInfo securityUserInfo = new SecurityUserInfo();
                securityUserInfo.setExpireDate(now + EXPIRE_DELTA);
                securityUserInfo.setId(userDetails.getId());
                List<DBHouse> houses = houseDAO.fetchByDBOwnerId(userDetails.getId());
                List<String> housesId = houses.stream().map(DBHouse::getId).collect(Collectors.toList());
                securityUserInfo.setOwnedHouses(housesId);
                return securityUserInfo;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}
