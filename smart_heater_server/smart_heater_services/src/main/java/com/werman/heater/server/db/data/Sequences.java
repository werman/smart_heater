/**
 * This class is generated by jOOQ
 */
package com.werman.heater.server.db.data;


import com.werman.heater.server.db.data.DBPublic;

import javax.annotation.Generated;

import org.jooq.Sequence;
import org.jooq.impl.SequenceImpl;


/**
 * Convenience access to all sequences in public
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.0"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Sequences {

	/**
	 * The sequence <code>public.sensor_id_seq</code>
	 */
	public static final Sequence<Long> SENSOR_ID_SEQ = new SequenceImpl<Long>("sensor_id_seq", DBPublic.PUBLIC, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

	/**
	 * The sequence <code>public.user_details_id_seq</code>
	 */
	public static final Sequence<Long> USER_DETAILS_ID_SEQ = new SequenceImpl<Long>("user_details_id_seq", DBPublic.PUBLIC, org.jooq.impl.SQLDataType.BIGINT.nullable(false));
}
