package com.werman.heater.server.services.devices;

import com.werman.heater.server.db.data.enums.DBDeviceType;
import com.werman.heater.server.services.devices.commands.*;
import com.werman.heater.server.services.devices.responses.RegisterDeviceResult;
import com.werman.heater.server.services.security.RestrictUnauthorized;
import com.werman.heater.server.services.security.restrictions.HouseOwnerRestriction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController(value = "RemoteHouseController")
@RequestMapping(value = "/api/house/devices/")
public class RemoteHouseController {

    @Autowired
    private RemoteDevicesManager remoteDevicesManager;

    @Autowired
    private DevicesService devicesService;

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "register", method = RequestMethod.POST)
    public RegisterDeviceResult registerDevice(@RequestParam String houseId,
                                               @RequestParam String deviceId,
                                               @RequestParam String description) {
        return remoteDevicesManager.registerDevice(houseId, deviceId, DBDeviceType.other, description);
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "poweron", method = RequestMethod.POST)
    public DeferredResult<RemoteCommandResult>
    powerOnHeatSource(@RequestParam String houseId,
                      @RequestParam String deviceId) {
        DeferredResult<RemoteCommandResult> result = new DeferredResult<>();

        RemoteDeviceCommand command = PowerOnCommand.create(deviceId);
        remoteDevicesManager.sendCommandToUsersHouse(houseId, command, result);

        return result;
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "poweroff", method = RequestMethod.POST)
    public DeferredResult<RemoteCommandResult>
    powerOffHeatSource(@RequestParam String houseId,
                       @RequestParam String deviceId) {
        DeferredResult<RemoteCommandResult> result = new DeferredResult<>();

        RemoteDeviceCommand command = PowerOffCommand.create(deviceId);
        remoteDevicesManager.sendCommandToUsersHouse(houseId, command, result);

        return result;
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "target_value", method = RequestMethod.POST)
    public DeferredResult<RemoteCommandResult>
    setTargetTemperature(@RequestParam String houseId,
                         @RequestParam String deviceId,
                         @RequestParam Float targetValue) {
        DeferredResult<RemoteCommandResult> result = new DeferredResult<>();

        RemoteDeviceCommand command = ChangeTargetTemperatureCommand.create(deviceId, targetValue);
        remoteDevicesManager.sendCommandToUsersHouse(houseId, command, result);

        devicesService.changeTargetValue(houseId, deviceId, targetValue);

        return result;
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "set_state", method = RequestMethod.POST)
    public void setState(@RequestParam String houseId,
                         @RequestParam String deviceId,
                         @RequestParam String state) {
        devicesService.setDeviceState(houseId, deviceId, state);
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "change_name", method = RequestMethod.POST)
    public void changeName(@RequestParam String houseId,
                         @RequestParam String deviceId,
                         @RequestParam String newName) {
        devicesService.changeDeviceName(houseId, deviceId, newName);
    }
}
