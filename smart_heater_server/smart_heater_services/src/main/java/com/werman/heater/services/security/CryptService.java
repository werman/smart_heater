package com.werman.heater.services.security;

import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CryptService {

    private MessageDigest md;

    public CryptService() {
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CryptService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getPasswordHash(String password) {
        byte[] passBytes = password.getBytes();
        md.reset();
        byte[] digested = md.digest(passBytes);
        StringBuilder sb = new StringBuilder();
        for (byte aDigested : digested) {
            sb.append(Integer.toHexString(0xff & aDigested));
        }
        return sb.toString();
    }
}
