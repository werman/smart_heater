package com.werman.heater.server.services.init;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.werman.heater.server.services.init.data.HouseInfo;
import com.werman.heater.server.services.security.RestrictUnauthorized;
import com.werman.heater.server.services.security.SecurityUserInfo;
import com.werman.heater.server.services.security.UserAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("InitDataController")
@RequestMapping(value = "/api/init")
public class InitDataController {

    @Autowired
    private InitDataService initDataService;

    @RestrictUnauthorized
    @RequestMapping(value = "", method = RequestMethod.GET)
    public InitDataResponse getInitData(@UserAuth SecurityUserInfo securityUserInfo) {
        InitDataResponse response = new InitDataResponse();
        response.setHouses(initDataService.getUserHousesDetails(securityUserInfo.getId()));
        return response;
    }


    static class InitDataResponse {
        @JsonProperty("houses")
        private List<HouseInfo> houses;

        public List<HouseInfo> getHouses() {
            return houses;
        }

        public void setHouses(List<HouseInfo> houses) {
            this.houses = houses;
        }
    }

}
