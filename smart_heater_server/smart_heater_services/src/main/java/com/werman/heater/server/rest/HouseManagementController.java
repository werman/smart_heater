package com.werman.heater.server.rest;

import com.werman.heater.server.services.house.HouseManagementService;
import com.werman.heater.server.services.security.JwtManager;
import com.werman.heater.server.services.security.RestrictUnauthorized;
import com.werman.heater.server.services.security.SecurityUserInfo;
import com.werman.heater.server.services.security.UserAuth;
import com.werman.heater.server.services.security.responses.LoginResponse;
import com.werman.heater.server.services.security.restrictions.HouseOwnerRestriction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("HouseManagementController")
@RequestMapping(value = "/api/house")
public class HouseManagementController {

    @Autowired
    private JwtManager jwtManager;

    @Autowired
    private HouseManagementService houseManagementService;

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/endpoint", method = RequestMethod.POST)
    public LoginResponse registerHouseEndpoint(@RequestParam String houseId,
                                               @RequestParam String url,
                                               @RequestParam String clientToken,
                                               @UserAuth SecurityUserInfo securityUserInfo) {
        boolean success = houseManagementService.registerHouseEndpoint(securityUserInfo, houseId, url, clientToken);
        if (success) {
            securityUserInfo.getOwnedHouses().add(houseId);
            String newToken = jwtManager.createTokenForUser(securityUserInfo);
            return new LoginResponse(newToken, "", "");
        } else {
            return null;
        }
    }

    @RestrictUnauthorized(restrictions = {HouseOwnerRestriction.class})
    @RequestMapping(value = "/name", method = RequestMethod.POST)
    public void changeName(@RequestParam String houseId,
                           @RequestParam String newName) {
        houseManagementService.changeName(houseId, newName);
    }
}
