package com.werman.heater.services.security;

import com.werman.heater.db.dao.UserDAO;
import com.werman.heater.db.data.tables.pojos.DBUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private CryptService cryptService;

    public boolean register(String email, String password) {
        boolean alreadyExists = userDAO.isUserWithEmailExists(email);

        if (alreadyExists) {
            return false;
        }

        String passwordHash = cryptService.getPasswordHash(password);

        DBUserDetails user = new DBUserDetails();
        user.setEmail(email);
        user.setPasswordHash(passwordHash);

        userDAO.insert(user);

        return true;
    }

    public DBUserDetails getUserDetails(String email) {
        return userDAO.findByEmail(email);
    }
}
