package com.werman.heater.server.services.devices.commands;

public class ChangeTargetTemperatureCommand extends RemoteDeviceCommand {

    private ChangeTargetTemperatureCommand() {
        super();
    }

    public static RemoteDeviceCommand create(String deviceId, Float targetTemperature) {
        ChangeTargetTemperatureCommand command = new ChangeTargetTemperatureCommand();
        command.setMethodName("change_target_temperature");
        command.addParam("id", deviceId);
        command.addParam("target", targetTemperature.toString());
        return command;
    }
}
