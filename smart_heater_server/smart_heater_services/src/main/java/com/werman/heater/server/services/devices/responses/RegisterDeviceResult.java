package com.werman.heater.server.services.devices.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterDeviceResult {

    @JsonProperty("device_id")
    private String deviceId;
    @JsonProperty("result")
    private String result;

    public RegisterDeviceResult(String deviceId, String result) {
        this.deviceId = deviceId;
        this.result = result;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
