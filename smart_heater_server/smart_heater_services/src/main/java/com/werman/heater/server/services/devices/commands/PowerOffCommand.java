package com.werman.heater.server.services.devices.commands;

public class PowerOffCommand extends RemoteDeviceCommand {

    private PowerOffCommand() {
        super();
    }

    public static RemoteDeviceCommand create(String deviceId) {
        PowerOffCommand command = new PowerOffCommand();
        command.setMethodName("power_off");
        command.addParam("id", deviceId);
        return command;
    }
}
