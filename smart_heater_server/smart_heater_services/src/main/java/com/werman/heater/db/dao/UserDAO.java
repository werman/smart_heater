package com.werman.heater.db.dao;

import com.werman.heater.db.data.tables.DBUserDetailsTable;
import com.werman.heater.db.data.tables.daos.DBUserDetailsDao;
import com.werman.heater.db.data.tables.pojos.DBUserDetails;
import org.springframework.stereotype.Service;

import static org.jooq.impl.DSL.selectOne;
import static org.jooq.impl.DSL.using;

@Service
public class UserDAO extends DBUserDetailsDao {

    private final DBUserDetailsTable USER = DBUserDetailsTable.USER_DETAILS;

    public boolean isUserWithEmailExists(String email) {
        return using(configuration())
                       .fetchExists(
                                           selectOne()
                                                   .from(USER)
                                                   .where(USER.EMAIL.eq(email)));
    }

    public DBUserDetails findByEmail(String email) {
        return using(configuration())
                .fetchOne(USER, USER.EMAIL.eq(email))
                       .into(DBUserDetails.class);
    }
}
