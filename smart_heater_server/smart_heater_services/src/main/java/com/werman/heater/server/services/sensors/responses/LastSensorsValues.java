package com.werman.heater.server.services.sensors.responses;

import java.util.Map;

public class LastSensorsValues {

    private Map<Integer, Float> values;

    public Map<Integer, Float> getValues() {
        return values;
    }

    public void setValues(Map<Integer, Float> values) {
        this.values = values;
    }
}
