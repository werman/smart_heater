package com.werman.heater.services.security;

import com.werman.heater.services.security.restrictions.Restriction;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class AuthorizedMethodInterceptor extends HandlerInterceptorAdapter {

    private Map<Class<? extends Restriction>, Restriction> restrictionClassCache = new HashMap<Class<? extends Restriction>, Restriction>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod methodHandler = (HandlerMethod) handler;
        RestrictUnauthorized annotation = methodHandler.getMethodAnnotation(RestrictUnauthorized.class);
        if (annotation != null) {
            SecurityUserInfo securityUserInfo = (SecurityUserInfo) request.getAttribute("securityUser");
            if (securityUserInfo == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            } else {
                for (Class<? extends Restriction> restrictionClass : annotation.restrictions()) {
                    Restriction restriction = restrictionClassCache.get(restrictionClass);
                    if (restriction == null) {
                        restriction = restrictionClass.newInstance();
                        restrictionClassCache.put(restrictionClass, restriction);
                    }

                    if (!restriction.hasAccess(request, securityUserInfo)) {
                        response.sendError(HttpServletResponse.SC_FORBIDDEN);
                        return false;
                    }
                }
                return true;
            }
        }
        return true;

    }
}
