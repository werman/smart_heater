drop schema public cascade;
create schema public;

CREATE TABLE user_details
(
  id SERIAL PRIMARY KEY,
  email VARCHAR(255) NOT NULL,
  password_hash VARCHAR(32) NOT NULL
);

CREATE TABLE house
(
  id VARCHAR(32) PRIMARY KEY,
--   square FLOAT4,
  owner_id INTEGER NOT NULL,
  service_address VARCHAR(255),
  service_token VARCHAR(512),
  name VARCHAR(255)
--   rooms JSON[]
);

CREATE TYPE sensor_type AS ENUM ('temperature', 'humidity');

CREATE TABLE sensor
(
  id SERIAL PRIMARY KEY,
  identifier VARCHAR(64),
  house_id VARCHAR(32) NOT NULL,
  type sensor_type,
  name VARCHAR(128),
  last_value FLOAT4,
  last_measure_time TIMESTAMP WITH TIME ZONE
);

CREATE TABLE sensor_history_entry
(
  sensor_id INTEGER,
  measure_time TIMESTAMP WITH TIME ZONE,
  value FLOAT4,
  PRIMARY KEY(sensor_id, measure_time)
);

CREATE TYPE device_type AS ENUM ('floor', 'infrared', 'radiator', 'fan', 'other');

CREATE TABLE device
(
  id VARCHAR(32) PRIMARY KEY,
--   identifier VARCHAR(64),
  house_id VARCHAR(32) NOT NULL,
  type device_type,
  name VARCHAR(128),
  target_value FLOAT4,
  turned_on BOOLEAN
);

CREATE TABLE device_history
(

);
